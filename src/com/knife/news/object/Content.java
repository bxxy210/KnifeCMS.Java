package com.knife.news.object;


public class Content {
	private String id;

	private String newsid;

	private int page;

	private String content;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNewsid() {
		return newsid;
	}

	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public Content(com.knife.news.model.Content comm){
		this.id=comm.getId();
		this.newsid=comm.getNewsid();
		this.page=comm.getPage();
		this.content=comm.getContent();
	}
}
