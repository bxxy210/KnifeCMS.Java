package com.knife.news.object;


/**
 * 供应商
 * @author Knife
 *
 */
public class Supplier {
	private String id;
	private String user;
	private String name;
	private String sid;
	private String sname;
	private String card;
	private String stype;
	private String tel;
	private String email;
	private String message;
	private String discount;
	private String license;
	private String logo;
	
	public Supplier(){
	}
	
	public Supplier(com.knife.news.model.Supplier s){
		this.id=s.getId();
		this.user=s.getUser();
		this.name=s.getName();
		this.sid=s.getSid();
		this.sname=s.getSname();
		this.card=s.getCard();
		this.stype=s.getStype();
		this.tel=s.getTel();
		this.discount=s.getDiscount();
		this.email=s.getEmail();
		this.license=s.getLicense();
		this.logo=s.getLogo();
		this.message=s.getMessage();
	}
	
	/**
	 * 取得编号
	 * @return 编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置编号
	 * @param id 编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得用户
	 * @return 用户
	 */
	public String getUser() {
		return user;
	}

	/**
	 * 设置用户
	 * @param user 用户
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * 取得名称
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 取得供应商ID
	 * @return ID
	 */
	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getCard() {
		return card;
	}

	public void setCard(String card) {
		this.card = card;
	}

	public String getStype() {
		return stype;
	}

	public void setStype(String stype) {
		this.stype = stype;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
}
