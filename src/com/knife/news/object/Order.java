package com.knife.news.object;

import java.util.Date;

/**
 * 订单对象
 * @author Knife
 *
 */
public class Order {
	private String id;
	private String user;
	private String title;
	private String goods;
	private int money;
	private Date date;
	private int status;
	
	public Order(com.knife.news.model.Order order){
		this.id=order.getId();
		this.user=order.getUser();
		this.title=order.getTitle();
		this.goods=order.getGoods();
		this.money=order.getMoney();
		this.date=order.getDate();
		this.status=order.getStatus();
	}
	
	/**
	 * 取得订单编号
	 * @return 订单编号
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * 设置订单编号
	 * @param id 订单编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 取得订单用户
	 * @return 订单用户
	 */
	public String getUser() {
		return user;
	}
	
	/**
	 * 设置订单用户
	 * @param user 订单用户
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	/**
	 * 取得订单货物
	 * @return 订单货物
	 */
	public String getGoods() {
		return goods;
	}
	
	/**
	 * 设置订单货物
	 * @param goods 订单货物
	 */
	public void setGoods(String goods) {
		this.goods = goods;
	}
	
	/**
	 * 取得订单价格
	 * @return 订单价格
	 */
	public int getMoney() {
		return money;
	}
	
	/**
	 * 设置订单价格
	 * @param money 订单价格
	 */
	public void setMoney(int money) {
		this.money = money;
	}
	
	/**
	 * 取得订单日期
	 * @return 订单日期
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * 设置订单日期
	 * @param date 订单日期
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * 取得订单状态
	 * @return 订单状态
	 */
	public int getStatus() {
		return status;
	}
	
	/**
	 * 设置订单价格
	 * @param status 订单价格
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}