package com.knife.news.object;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import com.knife.web.Globals;
//import com.sun.org.apache.xml.internal.security.keys.content.RetrievalMethod;

/**
 * 工作流node
 * @author jlm
 *
 */
public class FlowNode {
	private String id;
	private String name;
	private int type;
	private int step;
	private String workflow_id;
	private String init_function;
	private String run_function;
	private String trans_function;
	private String save_function;
	private String prev_node;
	private String next_node;
	private String executor;
	private int execute_type;
	private int remind;
	private String field;
	private int max_day;
	private int status ;
	
	public FlowNode(){
		
	}
	public FlowNode(com.knife.news.model.FlowNode custom){
		this.id		= custom.getId();
		this.name	= custom.getName();
		this.type	= custom.getType();
		this.step	= custom.getStep();
		this.workflow_id	= custom.getWorkflow_id();
		this.init_function  = custom.getInit_function();
		this.run_function  = custom.getRun_function();
		this.trans_function = custom.getTrans_function();
		this.save_function = custom.getSave_function();
		this.prev_node = custom.getPrev_node();
		this.next_node = custom.getNext_node();
		this.executor = custom.getExecutor();
		this.execute_type = custom.getExecute_type();
		this.remind = custom.getRemind();
		this.field = custom.getField();
		this.max_day = custom.getMax_day();
		this.status = custom.getStatus();
	}
	
	/**
	 * 取得编号
	 * @return 编号
	 */
	public String getId(){
		return id ;
	}
	/**
	 * 设置编号
	 * @return id 编号
	 */
	public void setId(String id){
		this.id = id ;
	}
	
	/**
	 * 取得工作流节点类型
	 * @return 节点类型     1表示人为决策， 2 自动处理（直接执行execute_function）， 3 等待外部响应（例如外部WS触发），4分支，5汇总，6结束节点（此节点执行时候自动终止进程）
	 */
	public int getType(){
		return type ;
	}
	
	/**
	 * 设置工作流节点类型
	 * @return type 节点类型   1表示人为决策， 2 自动处理（直接执行execute_function）， 3 等待外部响应（例如外部WS触发），4分支，5汇总，6结束节点（此节点执行时候自动终止进程）
	 */
	public void setType(int type){
		this.type = type ;
	}
	
	/**
	 * 取得节点名称
	 * @return 节点名称
	 */
	public String getName(){
		return name ;
	}
	
	/**
	 * 设置节点名称
	 * @return name 节点名称
	 */
	public void setName(String name){
		this.name = name ;
	}
	
	/**
	 * 取得节点序号
	 * @return 节点序号
	 */
	public int getStep(){
		return step ;
	}
	
	/**
	 * 设置节点序号
	 * @return step 节点序号
	 */
	public void setStep(int step){
		this.step = step ;
	}
	
	/**
	 * 取得所属的工作流id
	 * @return 工作流id
	 */
	public String getWorkflow_id(){
		return workflow_id;
	}
	
	/**
	 * 设置工作流id
	 * @return id 工作流id
	 */
	public void setWorkflow_id(String workflow_id){
		this.workflow_id = workflow_id ;
	}
	
	/**
	 * 取得流程运行的初始方法
	 * @return 流程运行的初始方法
	 */
	public String getInit_function(){
		return init_function ;
	}
	
	/**
	 * 设置流程运行的初始方法
	 * @return init_function 流程运行的初始方法
	 */
	public void setInit_function(String init_function){
		this.init_function = init_function ;
	}
	
	/**
	 * 取得流程运行方法
	 * @return 流程运行方法
	 */
	public String getRun_function(){
		return run_function ;
	}
	
	/**
	 * 设置流程运行方法
	 * @return run_function 流程运行方法
	 */
	public void setRun_function(String run_function){
		this.run_function = run_function ;
	}
	
	/**
	 *
	 */
	public String getTrans_function(){
		return trans_function ;
	}
	
	/**
	 * 
	 */
	public void setTrans_function(String trans_function){
		this.trans_function = trans_function ;
	}
	
	/**
	 * 取得流程的保存处理方法
	 * @return 流程的保存处理方法
	 */
	public String getSave_function(){
		return save_function ;
	}
	
	/**
	 * 设置流程的保存处理方法
	 * @return save_function 流程的保存处理方法
	 */
	public void setSave_function(String save_function){
		this.save_function = save_function ;
	}
	
	/**
	 * 取得前一个节点
	 * @return 前一个节点
	 */
	public String getPrev_node(){
		return prev_node ;
	}
	
	/**
	 * 设置前一个节点
	 * @return prev_node
	 */
	public void setPrev_node(String prev_node){
		this.prev_node = prev_node ;
	}
	
	/**
	 * 取得下一个节点
	 * @return 下一个节点
	 */
	public String getNext_node(){
		return next_node ;
	}
	
	/**
	 * 设置下一个节点
	 * @return next_node 下一个节点
	 */
	public void setNext_node(String next_node){
		this.next_node = next_node ;
	}
	
	/**
	 * 取得执行角色，组，人
	 * @return 执行角色，组，人
	 */
	public String getExecutor(){
		return executor ;
	}
	
	/**
	 * 设置执行角色，组，人
	 * @return  executor 执行角色，组，人
	 */
	public void setExecutor(String executor){
		this.executor = executor ;
	}
	
	/**
	 * 取得执行的方式
	 * @return 执行的方式    0需所有人执行 ， 1 只需一人执行
	 */
	public int getExecute_type(){
		return execute_type ;
	}
	
	/**
	 * 设置执行的方式
	 * @return execute_type 执行方式
	 */
	public void setExecute_type(int execute_type){
		this.execute_type = execute_type ;
	}
	
	/**
	 * 取得提醒执行人的方式
	 * @return 提醒执行人的方式   0表示不提醒 ， 1表示邮件方式 ，2表示是短信方式，3表示邮件+短信的方式
	 */
	public int getRemind(){
		return remind ;
	}
	
	/**
	 * 设置提醒执行人的方式
	 * @return remind 提醒执行人的方法
	 */
	public void setRemind(int remind){
		this.remind = remind ;
	}
	
	/**
	 * 取得可编辑的字段
	 * @return 可编辑的字段
	 */
	public String getField(){
		return field ;
	}
	
	/**
	 * 设置可编辑的字段
	 * @return field 可编辑的字段
	 */
	public void setField(String field){
		this.field = field ;
	}
	
	/**
	 * 取得最长的有效时间（天）
	 * @return 最长有效时间（天）
	 */
	public int getMax_day(){
		return max_day ;
	}
	
	/**
	 * 设置最长的有效时间（天）
	 * @return max_day 最长的有效时间（天）
	 */
	public void setMax_day(int max_day){
		this.max_day = max_day ;
	}
	
	/**
	 * 
	 * 
	 */
	public int getStatus(){
		return status;
	}
	
	/**
	 * 
	 * 
	 */
	public void setStatus(int status){
		this.status = status ;
	}
}
