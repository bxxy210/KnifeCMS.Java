package com.knife.news.object;

import java.util.Date;

/**
 * 日志对象
 * @author Knife
 *
 */
public class Logs {
	private String id;
	private String content;
	private Date date;
	
	public Logs(String content){
		this.id=new com.knife.news.model.Logs().getId();
		this.content=content;
		this.date=new Date();
	}
	
	public Logs(com.knife.news.model.Logs log){
		this.id=log.getId();
		this.content=log.getContent();
		this.date=log.getDate();
	}
	
	/**
	 * 取得日志编号
	 * @return 日志编号
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * 设置日志编号
	 * @param id 日志编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 取得日志内容
	 * @return 日志内容
	 */
	public String getContent() {
		return content;
	}
	
	/**
	 * 设置日志内容
	 * @param content 日志内容
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 取得日志日期
	 * @return 日志日期
	 */
	public Date getDate() {
		return date;
	}
	
	/**
	 * 设置日志日期
	 * @param date 日志日期
	 */
	public void setDate(Date date) {
		this.date = date;
	}
}