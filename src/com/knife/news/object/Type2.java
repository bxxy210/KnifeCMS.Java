package com.knife.news.object;

/**
 * 二维分类对象
 * @author Knife
 *
 */
public class Type2 {
	private String id;
	private String name;
	
	public Type2(){}
	
	/**
	 * 构造方法，将数据模型转换为对象
	 * @param atype2 分类的数据模型对象
	 */
	public Type2(com.knife.news.model.Type2 atype2){
		this.id=atype2.getId();
		this.name=atype2.getName();
	}
	
	/**
	 * 取得分类编号
	 * @return 分类编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置分类编号
	 * @param id 分类编号
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 取得分类名称
	 * @return 分类名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置分类名称
	 * @param name 分类名称
	 */
	public void setName(String name) {
		this.name = name;
	}
}