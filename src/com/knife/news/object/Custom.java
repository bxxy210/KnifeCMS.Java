package com.knife.news.object;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.knife.news.logic.SiteService;
import com.knife.news.logic.impl.SiteServiceImpl;
import com.knife.web.Globals;

/**
 * 任意文件
 * @author Administrator
 *
 */
public class Custom {
	private String id;
	private String name;
	private String site;
	private Date date;
	private File file;
	private String url;
	private int status;
	private String description;
	private String size;
	
	private SiteService siteDAO=SiteServiceImpl.getInstance();
	
	public Custom(){}
	
	public Custom(com.knife.news.model.Custom custom){
		this.id		= custom.getId();
		this.date	= custom.getDate();
		this.site	= custom.getSite();
		this.name	= custom.getName();
		this.url	= custom.getUrl();
		if(this.url!=null){
			this.file=new File(Globals.APP_BASE_DIR+this.url);
		}
		this.status	= custom.getStatus();
		if(this.file!=null){
			if(this.file.exists()){
				long sizenum=this.file.length();
				this.size=FileUtils.byteCountToDisplaySize(sizenum);
			}
		}
		this.description = custom.getDescription();
	}

	/**
	 * 取得编号
	 * @return 编号
	 */
	public String getId() {
		return id;
	}

	/**
	 * 设置编号
	 * @param id 编号
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 取得发布时间
	 * @return 发布时间
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * 设置发布时间
	 * @param date 发布时间
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * 取得名称
	 * @return 名称
	 */
	public String getName() {
		return name;
	}

	/**
	 * 设置名称
	 * @param name 名称
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 取得所处站点
	 * @return 站点编号
	 */
	public String getSite() {
		return site;
	}

	/**
	 * 设置所处站点
	 * @param site 站点编号
	 */
	public void setSite(String site) {
		this.site = site;
	}

	/**
	 * 取得zip文件
	 * @return 文件对象
	 */
	public File getFile() {
		return file;
	}

	/**
	 * 设置zip文件
	 * @param file 文件对象
	 */
	public void setFile(File file) {
		this.file = file;
	}

	/**
	 * 取得zip文件url
	 * @return 文件url地址
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 设置zip文件url
	 * @param url 文件url地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * 取得文件状态
	 * @return 文件状态:1已发布,0未发布
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * 设置文件状态
	 * @param status 文件状态:1已发布,0未发布
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 取得文件大小
	 * @return 文件大小
	 */
	public String getSize() {
		return size;
	}

	/**
	 * 设置文件大小
	 * @param size 文件大小
	 */
	public void setSize(String size) {
		this.size = size;
	}
	
	/**
	 * 取得文件描述
	 * @return 文件描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置文件描述
	 * @param description 文件描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 取得站点名称
	 * @return 站点名称
	 */
	public String getSiteName(){
		try{
			Site site=siteDAO.getSiteById(this.site);
			return site.getName();
		}catch(Exception e){
			return null;
		}
	}
	
	/**
	 * 取得zip文件中文件列表(不支持子目录)
	 * @return 任意文件项列表
	 */
	public List<CustomFile> getFiles(){
		List<CustomFile> cfs=new ArrayList<CustomFile>();
		File dir=new File(this.getFile().getAbsolutePath().replace(".zip", "/"));
		if(dir.exists() && dir.isDirectory()){
			for(File in:dir.listFiles()){
				CustomFile cf=new CustomFile();
				if(in.isFile()){
					cf.setName(in.getName());
					cf.setUrl(this.getUrl().replace(".zip", "/")+in.getName());
					cfs.add(cf);
				}
			}
		}
		return cfs;
	}
}
