package com.knife.news.object;

/**
 * 任意文件项
 * @author Smith
 *
 */
public class CustomFile {
	private String name;
	private String url;
	
	/**
	 * 取得文件名
	 * @return 文件名
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 设置文件名
	 * @param name 文件名
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 取得文件url路径
	 * @return 文件url路径
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * 设置文件url路径
	 * @param url 文件url路径
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
