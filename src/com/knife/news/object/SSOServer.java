package com.knife.news.object;

import com.knife.tools.WebXML;

/**
 * 单点登录服务器
 * @author Knife
 *
 */
public class SSOServer {
	private String name;
	private String url;
	private int type;
	
	/**
	 * 取得单点服务器名
	 * @return 单点服务器名
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 设置单点服务器名
	 * @param name 单点服务器名
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 取得单点服务器地址
	 * @return 单点服务器地址
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * 设置单点服务器地址
	 * @param url 单点服务器地址
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public SSOServer(){
		WebXML webxml=new WebXML();
		this.name=webxml.read("serverName");
		this.url=webxml.read("casServerLoginUrl");
		this.type=Integer.parseInt(webxml.read("serverType"));
	}
}
