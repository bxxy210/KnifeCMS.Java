package com.knife.news.stat;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

/**
 * 字符编码filter
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company: </p>
 * @author yangyandong
 * @version 1.0
 */
@SuppressWarnings("serial")
public class EncodingFilter extends HttpServlet implements Filter{
    private FilterConfig filterConfig;
    private String encoding = null;
    protected boolean ignore = true;
    
    public FilterConfig getFilterConfig(){
    	return filterConfig;
    }

    public void init(FilterConfig filterConfig) throws ServletException{
        this.filterConfig = filterConfig;
        this.encoding = filterConfig.getInitParameter("encoding");
        String value = filterConfig.getInitParameter("ignore");
        if (value == null){
            this.ignore = true;
        } else if (value.equalsIgnoreCase("true")) {
            this.ignore = true;
        } else if (value.equalsIgnoreCase("yes")) {
            this.ignore = true;
        } else {
            this.ignore = false;
        }
    }

    //Process the request/response pair
    public void doFilter(ServletRequest request,ServletResponse response,
                         FilterChain filterChain){
        try{
            if (ignore || (request.getCharacterEncoding() == null)) {
                 String encoding = selectEncoding(request);
                 if (encoding != null)
                     request.setCharacterEncoding(encoding);
             }
            filterChain.doFilter(request,response);
        } catch(Exception sx){
            sx.getMessage();
        }

    }

    private String selectEncoding(ServletRequest request) {
        return (this.encoding);
    }

    //Clean up resources
    public void destroy(){
        encoding = null;
        filterConfig = null;
    }
}
