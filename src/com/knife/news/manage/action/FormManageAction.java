package com.knife.news.manage.action;

import java.util.ArrayList;
import java.util.List;

import com.knife.news.logic.impl.FormServiceImpl;
import com.knife.news.model.Form;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class FormManageAction extends ManageAction{
	private FormServiceImpl formdao = FormServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}
	
	public Page doList(WebForm form, Module module) {
		List<Form> typeList = formdao.getAllForms();
		int rows = typeList.size();// 分页开始
		int pageSize = 15;
		int currentPage = 1;
		int frontPage = 0;
		int nextPage = 2;
		List<Form> firstTypes = new ArrayList<Form>();
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));
		firstTypes = formdao.getFormsBySql("id!=''", null,0, pageSize);
		form.addResult("rows", rows);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("currentPage", currentPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("formList", firstTypes);
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Form> typeList = formdao.getAllForms();
		int rows = typeList.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Form> firstTypes = new ArrayList<Form>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			firstTypes = formdao.getFormsBySql("id!=''", null, begin - 1, end);
		} else {
			firstTypes = formdao.getFormsBySql("id!=''", null, begin - 1, pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("formList", firstTypes);
		return module.findPage("list");
	}
	
	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}
	
	public Page doSave(WebForm form, Module module) {
		Form myform = (Form) form.toPo(Form.class);
		if (formdao.saveForm(myform)) {
			form.addResult("msg", "添加成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "添加失败！");
			return doList(form, module);
		}
	}
	
	public Page doEdit(WebForm form,Module module){
		String id = CommUtil.null2String(form.get("id"));
		if(id.equals("")){id="0";}
		Form aform = formdao.getFormById(id);
		form.addResult("action", "update");
		form.addResult("form", aform);
		return module.findPage("edit");
	}
	
	public Page doUpdate(WebForm form, Module module) {
		Form aform = (Form) form.toPo(Form.class);
		if (formdao.updateForm(aform)) {
			form.addResult("msg", "编辑成功！");
			form.addResult("id", aform.getId());
			return doEdit(form, module);
		} else {
			form.addResult("msg", "编辑失败！");
			form.addResult("id", aform.getId());
			return doEdit(form, module);
		}
	}
	
	public Page doDelete(WebForm form, Module module) {
		String id = (String) form.get("id");
		boolean result = false;
		if(id.indexOf(",")>0){
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Form aform = formdao.getFormById(ids[i]);
				result = formdao.delForm(aform);
			}
		}else{
			Form aform = formdao.getFormById(id);
			result = formdao.delForm(aform);
		}
		if (result) {
			form.addResult("msg", "删除成功！");
			return doList(form, module);
		} else {
			form.addResult("msg", "删除失败！");
			return doList(form, module);
		}
	}
}