package com.knife.news.manage.action;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.knife.news.logic.BillService;
import com.knife.news.logic.impl.BillServiceImpl;
import com.knife.news.object.Bill;
import com.knife.util.CommUtil;
import com.knife.web.Globals;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class BillManageAction extends ManageAction {

	BillService billDAO = BillServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doList(form, module);
	}

	public Page doList(WebForm form, Module module) {
		List<Bill> allfilts = billDAO.getAllBill();
		if (allfilts != null) {
			int rows = allfilts.size();// 分页开始
			int pageSize = 15;
			int currentPage = 1;
			int frontPage = 0;
			int nextPage = 2;
			int totalPage = (int) Math
					.ceil((float) (rows) / (float) (pageSize));
			List<Bill> firstfilts = billDAO.getBillBySql("id!=''", null, 0,
					pageSize);
			form.addResult("rows", rows);
			form.addResult("pageSize", pageSize);
			form.addResult("frontPage", frontPage);
			form.addResult("currentPage", currentPage);
			form.addResult("nextPage", nextPage);
			form.addResult("totalPage", totalPage);
			form.addResult("billList", firstfilts);
		}
		return module.findPage("list");
	}

	public Page doPage(WebForm form, Module module) {
		List<Bill> allfilts = billDAO.getAllBill();
		int rows = allfilts.size();
		int pageSize = 15;
		int paraPage = CommUtil.null2Int(form.get("page"));
		int frontPage = paraPage - 1;
		int nextPage = paraPage + 1;
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		int begin = (paraPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<Bill> filterList = new ArrayList<Bill>();
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		if (end < pageSize) {
			filterList = billDAO
					.getBillBySql("id!=''", null, begin - 1, end);
		} else {
			filterList = billDAO.getBillBySql("id!=''", null, begin - 1,
					pageSize);
		}
		form.addResult("currentPage", paraPage);
		form.addResult("totalPage", totalPage);
		form.addResult("rows", rows);
		form.addResult("billList", filterList);
		return module.findPage("list");
	}

	public Page doAdd(WebForm form, Module module) {
		form.addResult("action", "save");
		return module.findPage("edit");
	}

	public Page doEdit(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		Bill bill = billDAO.getBillById(id);
		form.addResult("bill", bill);
		form.addResult("action", "update");
		return module.findPage("edit");
	}
	
	public Page doRemoveFile(WebForm form, Module module){
		String id = CommUtil.null2String(form.get("id"));
		Bill bill = billDAO.getBillById(id);
		if(bill.getFile()!=null){
			if(bill.getFile().length()>0){
				File oldFile = new File(Globals.APP_BASE_DIR + bill.getFile());
				if(oldFile.exists()){
					oldFile.delete();
				}
				bill.setFile(null);
			}
		}
		if(billDAO.updateBill(bill)){
			form.addResult("msg", "删除成功");
		}else{
			form.addResult("msg", "删除失败");
		}
		form.addResult("bill", bill);
		form.addResult("action", "update");
		return doEdit(form, module);
	}

	public Page doDelete(WebForm form, Module module) {
		String id = CommUtil.null2String(form.get("id"));
		boolean result=false;
		if (id.indexOf(",") > 0) {
			String[] ids = id.split(",");
			for (int i = 0; i < ids.length; i++) {
				Bill bill = billDAO.getBillById(ids[i]);
				result = billDAO.delBill(bill);
			}
		} else {
			Bill bill = billDAO.getBillById(id);
			result = billDAO.delBill(bill);
		}
		if(result){
			form.addResult("msg", "删除成功");
		}else{
			form.addResult("msg", "删除失败");
		}
		return doList(form, module);
	}

	public Page doSave(WebForm form, Module module) {
		Bill bill = (Bill) form.toPo(Bill.class);
		//先上传账单
		FileItem file=(FileItem)form.get("upfile");
		if(file!=null){
			String filePath=Globals.APP_BASE_DIR+"upload"+File.separator+"open"+File.separator;
			String filename=file.getName();
			String newfilename=filename.substring(filename.lastIndexOf(File.separator)+1);
			try{
				file.write(new File(filePath+newfilename));
				bill.setFile("/upload/open/"+newfilename);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		if (billDAO.saveBill(bill)) {
			form.addResult("msg", "添加成功！");
		} else {
			form.addResult("msg", "添加失败！");
		}
		return doList(form, module);
	}

	public Page doUpdate(WebForm form, Module module) {
		Bill bill = (Bill) form.toPo(Bill.class);
		//先上传账单
		FileItem file=(FileItem)form.get("upfile");
		if(file!=null){
			//删除之前的账单
			if(bill.getFile()!=null){
				if(bill.getFile().length()>0){
					File oldFile = new File(Globals.APP_BASE_DIR + bill.getFile());
					if(oldFile.exists()){
						oldFile.delete();
					}
				}
			}
			String filePath=Globals.APP_BASE_DIR+"upload"+File.separator+"open"+File.separator;
			String filename=file.getName();
			String newfilename=filename.substring(filename.lastIndexOf(File.separator)+1);
			try{
				file.write(new File(filePath+newfilename));
				bill.setFile("/upload/open/"+newfilename);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		if (billDAO.updateBill(bill)) {
			form.addResult("msg", "编辑成功！");
		} else {
			form.addResult("msg", "编辑失败！");
		}
		return doList(form, module);
	}
}
