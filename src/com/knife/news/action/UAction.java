package com.knife.news.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.member.Usertype;
import com.knife.member.UsertypeDAO;
import com.knife.news.logic.CommentService;
import com.knife.news.logic.PicTypeService;
import com.knife.news.logic.impl.CommentServiceImpl;
import com.knife.news.logic.impl.PicTypeServiceImpl;
import com.knife.news.object.Comment;
import com.knife.news.object.PicType;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

/**
 * 未登录状态会员列表及空间的查看
 * @author Smith
 *
 */
public class UAction extends BaseCmdAction {
	private UserinfoDAO userDAO = new UserinfoDAO();
	private UsertypeDAO utDAO = new UsertypeDAO();
	private PicTypeService ptDAO = PicTypeServiceImpl.getInstance();
	//jlm
	private CommentService commentDAO = CommentServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		return doPage( form,  module);
	}
	
	// 作品展示
	public Page doView(WebForm form, Module module) {
		String ptid = CommUtil.null2String(form.get("ptid"));
		PicType thisPictype = ptDAO.getPicTypeById(ptid);
		Userinfo thisUser=userDAO.findById((long)thisPictype.getUserid());
		Usertype thisUsertype = utDAO.findById(thisUser.getIsfee());
		form.addResult("utDAO", utDAO);
		form.addResult("thisPictype", thisPictype);
		form.addResult("thisUsertype", thisUsertype);
		form.addResult("thisUser", thisUser);
		return new Page("show", "u_view.htm", "html", "/web/"
				+ thisSite.getTemplate() + "/");
	}

	// 空间展示
	public Page doShow(WebForm form, Module module) {
		int uid = CommUtil.null2Int(form.get("uid"));	
		Userinfo thisUser=userDAO.findById((long)uid);
		Usertype thisUsertype = utDAO.findById(thisUser.getIsfee());
		//访问计数加一
		thisUser.setCount(thisUser.getCount()+1);
		userDAO.update(thisUser);
		form.addResult("utDAO", utDAO);
		form.addResult("thisUsertype", thisUsertype);
		form.addResult("thisUser", thisUser);
		
		return new Page("show", "u_index.htm", "html", "/web/"
				+ thisSite.getTemplate() + "/");
	}

	public Page doPage(WebForm form, Module module){
		int utid=CommUtil.null2Int(form.get("utid"));
		Usertype thisUsertype = utDAO.findById(utid);
		form.addResult("utDAO", utDAO);
		form.addResult("userDAO",userDAO);
		form.addResult("thisUsertype", thisUsertype);
		List<Userinfo> users=new ArrayList<Userinfo>();
		int page_now=1;
		int page_prev=1;
		int page_next=1;
		int total_page=1;
		int page_size=20;
		page_now=CommUtil.null2Int(form.get("page"));
		if(page_now<=0){
			page_now=1;
		}
		try {
			users=userDAO.findPagedAllisfee(page_now,page_size,utid);
			total_page = userDAO.getTotalPageIsFee(page_size,utid);
		} catch (Exception e) {
		}
		if(page_now>1)page_prev=page_now-1;
		if(page_now<total_page)page_next=page_now+1;
		form.addResult("currentPage", page_now);
		form.addResult("totalPage", total_page);
		form.addResult("pageSize", page_size);
		form.addResult("frontPage", page_prev);
		form.addResult("nextPage", page_next);
		form.addResult("userList", users);
		form.addResult("utid", utid);
		return new Page("uPage", "u_list.htm","html", "/web/" + thisSite.getTemplate() + "/");
	}
	
	public Page doUlist(WebForm form, Module module){
		int utid=CommUtil.null2Int(form.get("utid"));
		
		Usertype thisUsertype = utDAO.findById(utid);
		form.addResult("utDAO", utDAO);
		
		List<Userinfo> users=new ArrayList<Userinfo>();
		int page_now=1;
		int page_prev=1;
		int page_next=1;
		int total_page=1;
		int page_size=8;
		page_now=CommUtil.null2Int(form.get("page"));
		if(page_now<=0){
			page_now=1;
		}
		try {
			if(utid==0){
				users=userDAO.findPagedisfee(page_now,page_size,utid);
				total_page = userDAO.getPageIsFee(page_size,utid);
			}
			else{
			users=userDAO.findPagedAllisfee(page_now,page_size,utid);
			total_page = userDAO.getTotalPageIsFee(page_size,utid);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(page_now>1)page_prev=page_now-1;
		if(page_now<total_page)page_next=page_now+1;
		form.addResult("currentPage", page_now);
		form.addResult("totalPage", total_page);
		form.addResult("pageSize", page_size);
		form.addResult("frontPage", page_prev);
		form.addResult("nextPage", page_next);
		form.addResult("userList", users);
		if(utid==0){
			return new Page("uPage", "d2.php","html", "/web/" + thisSite.getTemplate() + "/");
		}else{
		return new Page("uPage", "yanyuantest.php","html", "/web/" + thisSite.getTemplate() + "/");
		}
	}
	
	//jlm  
	public Page doCommend(WebForm form,Module module){
		int utid=CommUtil.null2Int(form.get("utid"));
		String uid = CommUtil.null2String(form.get("uid"));
		//Usertype thisUsertype = utDAO.findById(utid);
		//Userinfo thisUser=userDAO.findById((long)utid);
		int onlineuid = CommUtil.null2Int(onlineUser.getId());
		Comment comment = commentDAO.getCommentById(uid);
		
		form.addResult("tid", tid);
		String commenttext = CommUtil.null2String(form.get("comment"));
		comment.setComment(commenttext);
		
		form.addResult("utDAO", utDAO);
		form.addResult("userDAO", userDAO);
		form.addResult("uid", (long)onlineuid);
		//form.addResult("thisUsertype", thisUsertype);
		//form.addResult("thisUser", thisUser);
		//form.addResult("onlineUser", onlineUser);
		
		
		if (thisNews != null) {
			form.addResult("thisNews", thisNews);
			String newsid= thisNews.getId();
			Collection<Object> paras = new ArrayList<Object>();
			paras.add(thisNews.getId());
			if(thisSite.getCommcheck()==0){
				List<Comment> comments=commentDAO.getCommBySql("id!='' and k_news_id=? ", paras,0, -1);
				form.addResult("comments", comments);
			}else{
				List<Comment> comments=commentDAO.getCommBySql("id!='' and k_news_id=? and k_display=1", paras,0, -1);
				form.addResult("comments", comments);
			}
		}
			
		return new Page("ucommend", "news1-detail.htm","html", "/web/" + thisSite.getTemplate() + "/");
	}
	
}
