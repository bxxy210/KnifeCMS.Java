package com.knife.news.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
import com.knife.news.logic.OrderService;
import com.knife.news.logic.impl.OrderServiceImpl;
import com.knife.news.object.News;
import com.knife.news.object.Order;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class OrderAction extends BaseCmdAction {
	OrderService orderDAO = OrderServiceImpl.getInstance();

	public Page doInit(WebForm form, Module module) {
		// TODO 自动生成方法存根
		return doList(form,module);
	}
	
	public Page doList(WebForm form, Module module){
		String treenews=CommUtil.null2String(form.get("treenews"));
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(thisType.getId());
		String sql="k_display='1' and k_type=?";
		if(treenews.length()>0){
			paras.add(treenews);
			sql+=" and k_treenews=?";
			form.addResult("treenews", treenews);
		}
		List<News> allNews = newsDAO.getNews(sql,paras);
		if(allNews==null){allNews=new ArrayList<News>();}
		int rows = allNews.size();// 总数
		int pageSize = CommUtil.null2Int(thisType.getPagenum());// 每页20条
		int currentPage = 1;
		int frontPage = 1;
		int nextPage = 2;
		int totalPage = (int) Math.ceil((float) (rows) / (float) (pageSize));// 计算页数
		List<News> firstNews = newsDAO.getNewsBySql(sql+" order by length(k_order) desc,k_order desc", paras, 0, pageSize);
		if(firstNews==null){ firstNews = new ArrayList<News>();}
		form.addResult("rows", rows);
		form.addResult("currentPage", currentPage);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("totalPage", totalPage);
		form.addResult("newsList", firstNews);
		String ext="";
		try{
			ext = thisType.getType_template().substring(thisType.getType_template().lastIndexOf(".")+1);
		}catch(Exception e){}
		if(!ext.equalsIgnoreCase("url") && !ext.equalsIgnoreCase("json")&& !ext.equalsIgnoreCase("xml")){
			ext="html";
		}
		return new Page("typeIndex","/web/"+thisType.getType_template(),ext);
	}

	public Page doPage(WebForm form, Module module) {
		String treenews=CommUtil.null2String(form.get("treenews"));
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(thisType.getId());
		String sql="k_display='1' and k_type=?";
		if(treenews.length()>0){
			paras.add(treenews);
			sql+=" and k_treenews=?";
			form.addResult("treenews", treenews);
		}
		List<News> allNews = newsDAO.getNews(sql,paras);
		int rows = allNews.size();
		int pageSize = CommUtil.null2Int(thisType.getPagenum());
		int currentPage = CommUtil.null2Int(form.get("page"));
		int totalPage = (int) Math.ceil((float) rows / (float) pageSize);
		if(currentPage<1){currentPage=1;}
		if(currentPage>totalPage){currentPage=totalPage;}
		int frontPage = currentPage - 1;
		int nextPage = currentPage + 1;
		int begin = (currentPage - 1) * pageSize + 1;
		int end = rows - begin + 1;
		List<News> newsList = new ArrayList<News>();
		if (end < pageSize) {
			newsList = newsDAO.getNewsBySql(sql+" order by length(k_order) desc,k_order desc", paras, begin - 1, end);
		} else {
			newsList = newsDAO.getNewsBySql(sql+" order by length(k_order) desc,k_order desc", paras, begin - 1, pageSize);
		}
		//System.out.println("获取到的新闻条数是:"+newsList.size());
		form.addResult("currentPage", currentPage);
		form.addResult("totalPage", totalPage);
		form.addResult("pageSize", pageSize);
		form.addResult("frontPage", frontPage);
		form.addResult("nextPage", nextPage);
		form.addResult("rows", rows);
		form.addResult("newsList", newsList);
		String ext="";
		try{
			ext = thisType.getType_template().substring(thisType.getType_template().lastIndexOf(".")+1);
		}catch(Exception e){}
		if(!ext.equalsIgnoreCase("url") && !ext.equalsIgnoreCase("json")&& !ext.equalsIgnoreCase("xml")){
			ext="html";
		}
		return new Page("typeIndex","/web/"+thisType.getType_template(),ext);
	}
	
	//保存订单
	public Page doSave(WebForm form, Module module){
		Order order=new Order(new com.knife.news.model.Order());
		order.setUser(loginUser);
		order.setDate(new Date());
		String goods=CommUtil.null2String(form.get("goods"));
		if(goods.length()<=0){goods="null";}
		String title=CommUtil.null2String(form.get("title"));
		order.setGoods(goods);
		order.setTitle(title);
		order.setMoney(CommUtil.null2Int(form.get("money")));
		order.setStatus(1);
		boolean ret=orderDAO.saveOrder(order);
		if(ret){
			//扣除会员积分
			Userinfo ouser = new Userinfo();
			UserinfoDAO uiDAO = new UserinfoDAO();
			List ousers = uiDAO.findActiveByAcount(loginUser);
			if(ousers.size()>0){
				ouser = (Userinfo)ousers.get(0);
				if(ouser.getScore()>order.getMoney()){
					ouser.setScore(ouser.getScore()-order.getMoney());
					uiDAO.update(ouser);
					form.addResult("jvalue", "保存成功");
				}else{
					form.addResult("jvalue", "保存失败，积分不够！");
				}
			}else{
				form.addResult("jvalue", "错误，不存在的会员！");
			}
		}else{
			form.addResult("jvalue", "保存失败");
		}
		return module.findPage("ajax");
	}
}
