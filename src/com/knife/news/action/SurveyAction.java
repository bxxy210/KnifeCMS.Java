package com.knife.news.action;

import net.sf.json.JSONArray;

import com.knife.news.logic.SurveyDefineService;
import com.knife.news.logic.SurveyService;
import com.knife.news.logic.impl.SurveyDefineServiceImpl;
import com.knife.news.logic.impl.SurveyServiceImpl;
import com.knife.news.object.Survey;
import com.knife.news.object.SurveyDefine;
import com.knife.util.CommUtil;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;

public class SurveyAction extends BaseCmdAction{
	SurveyService surveyDAO = SurveyServiceImpl.getInstance();
	SurveyDefineService surveyDefineDAO = SurveyDefineServiceImpl.getInstance();
	private String surveyId;
	
	@Override
	public Page doInit(WebForm form, Module module) {
		// TODO Auto-generated method stub
		
		return new Page("surveylist", "surveylist.htm","html","/web/" + thisSite.getTemplate() + "/");
	}
	
	public Page doList(WebForm form, Module module){
		return null;
	}
	
	public Page doShow(WebForm form, Module module) {
		if(surveyId==null){
			surveyId=CommUtil.null2String(form.get("id"));
		}
		form.addResult("sid", surveyId);
		SurveyDefine surveyDefine = surveyDefineDAO.getSurveyDefineById(surveyId);
		form.addResult("surveyName", surveyDefine.getName());
		String fields="[" +surveyDefine.getXml() + "]";
		JSONArray fieldsArray = JSONArray.fromObject(fields);
		form.addResult("fields", fieldsArray);
		return new Page("survey", "survey.htm","html","/sys/");
	}

	public Page doSave(WebForm form, Module module) {
		Survey survey = (Survey) form.toPo(Survey.class);
		if(surveyDAO.saveSurvey(survey)) {
			form.addResult("msg", "提交成功！");
		} else {
			form.addResult("msg", "提交失败！");
		}
		surveyId=survey.getSid();
		return doShow(form,module);
	}
}
