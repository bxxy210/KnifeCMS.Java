package com.knife.news.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.knife.util.CommUtil;
import com.knife.web.ActionContext;
import com.knife.web.Module;
import com.knife.web.Page;
import com.knife.web.WebForm;
import com.knife.member.FindPoint;
import com.knife.member.PointDAO;
import com.knife.member.Point;
import com.knife.member.PointSum;
import com.knife.member.Userinfo;
import com.knife.member.UserinfoDAO;
//金客会会员积分内容
public class PointAction extends BaseCmdAction {
	@Override
	public Page doInit(WebForm form, Module module) {
		return doSummary(form,module);
	}

	public Page doSummary(WebForm form, Module module){
		
		return new Page("score", "user_point.htm","html","/web/" + thisSite.getTemplate() + "/");
	}
	
	public Page doList(WebForm form, Module module){
		//会员id
		String memguid = "";
		//积分项
		String pointtype =  CommUtil.null2String(form.get("pointtype"));
		if(onlineUser==null){
			return new Page("index", "index.htm","html","/web/" + thisSite.getTemplate() + "/");
		}
		memguid = onlineUser.getMemguid();
		form.addResult("user", onlineUser);
		//累计已兑换积分=累计奖励积分-累计待兑换积分
		Double ljydhpoint = onlineUser.getLjpoint()-onlineUser.getLjddhpoint();
		form.addResult("ljydhpoint",ljydhpoint );
		PointDAO pointDAO = new 	PointDAO();
		List<Point> pointList = new ArrayList<Point>();
		pointList = pointDAO.findByMemGUID(memguid);
		form.addResult("pointList",pointList);
		List<PointSum> list_p = new ArrayList<PointSum>();
		//通过jdbc方式获取会员积分信息
		 list_p = FindPoint.findPointMainByMemGUID(memguid);
		form.addResult("pointtypes",  list_p);
		if(pointtype=="" || pointtype==null){
			return new Page("point", "user_point.htm","html","/web/" + thisSite.getTemplate() + "/");
		}else{
			form.addResult("pointtype", pointtype);
			return new Page("pointdetail", "user_point_detail.htm","html","/web/" + thisSite.getTemplate() + "/");
		}
		
	}
}
