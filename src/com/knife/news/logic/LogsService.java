package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Logs;

/**
 * 日志接口
 * @author Knife
 *
 */
public interface LogsService {
	
	/**
	 * 保存日志
	 * @param log
	 * @return true保存成功,false保存失败
	 */
	boolean saveLogs(Logs log);
	
	/**
	 * 更新日志
	 * @param log
	 * @return true更新成功,false更新失败
	 */
	boolean updateLogs(Logs log);
	
	/**
	 * 按编号取得日志
	 * @param id 编号
	 * @return 日志对象
	 */
	Logs getLogsById(String id);
	
	/**
	 * 删除日志
	 * @param log
	 * @return true删除成功,false删除失败
	 */
	boolean delLogs(Logs log);
	
	/**
	 * 清空日志
	 * @return true清空成功,false清空失败
	 */
	boolean cleanLogs();
	
	/**
	 * 取得所有日志
	 * @return 日志列表
	 */
	List<Logs> getAllLogs();
	
	/**
	 * 按查询条件取得日志
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始地址
	 * @param end 结束地址
	 * @return 日志列表
	 */
	List<Logs> getLogsBySql(String sql, Collection<Object> paras, int begin, int end);
}
