package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Supplier;

/**
 * 供应商接口
 * @author Knife
 *
 */
public interface SupplierService {
	
	/**
	 * 取得所有供应商列表
	 * @return 供应商列表
	 */
	List<Supplier> getAllSupplier();
	
	/**
	 * 按条件取得供应商列表
	 * @param sql 查询条件
	 * @return 供应商列表
	 */
	List<Supplier> getSupplierBySql(String sql);
	
	/**
	 * 按条件取得指定范围的供应商列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 供应商列表
	 */
	List<Supplier> getSupplierBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按编号取得供应商
	 * @param id 供应商编号
	 * @return 供应商对象
	 */
	Supplier getSupplierById(String id);
	
	/**
	 * 按用户名密码取得供应商
	 * @param user 供应商编号
	 * @param pass 供应商密码
	 * @return 供应商对象
	 */
	Supplier getSupplierByUserAndPass(String user,String pass);
	
	/**
	 * 保存供应商
	 * @param supplier 供应商对象
	 * @return ture保存成功，false保存失败
	 */
	boolean saveSupplier(Supplier supplier);
	
	/**
	 * 更新供应商
	 * @param supplier 供应商对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateSupplier(Supplier supplier);
	
	/**
	 * 删除供应商
	 * @param supplier 供应商对象
	 * @return true删除成功，false删除失败
	 */
	boolean delSupplier(Supplier supplier);
}
