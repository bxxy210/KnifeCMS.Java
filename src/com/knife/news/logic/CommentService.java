package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Comment;

/**
 * 评论接口
 */
public interface CommentService {
	/**
	 * 保存评论
	 * @param comment 评论对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveComment(Comment comment);

	/**
	 * 更新评论
	 * @param comment 评论对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateComment(Comment comment);

	/**
	 * 删除评论
	 * @param comment 评论对象
	 * @return true删除成功，false删除失败
	 */
	boolean delComment(Comment comment);

	/**
	 * 取得所有评论
	 * @return 评论列表
	 */
	List<Comment> getComment();
	
	/**
	 * 取得指定文章的评论
	 * @param newsid
	 * @return
	 */
	List<Comment> getCommentByNewsid(String newsid);
	
	/**
	 * 按条件取得评论
	 * @param sql
	 * @param paras
	 * @return 评论列表
	 */
	List<Comment> getComment(String sql, Collection<Object> paras);
	
	/**
	 * 按条件取得一定范围内的评论
	 * @param sql
	 * @param paras
	 * @param begin
	 * @param end
	 * @return 评论列表
	 */
    List<Comment> getCommBySql(String sql,Collection<Object> paras,int begin,int end);
	Comment getCommentById(String id);
}
