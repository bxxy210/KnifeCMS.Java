package com.knife.news.logic;

import java.util.List;

import com.knife.news.object.Work;

public interface WorkService {

	public abstract Work getWorkById(String id,int tid);
	
	public abstract void auditWork(String id,int typeid);
	
	public abstract void deleteWork(String id,int typeid);
	
	public abstract List<Work> getWorksByType(int typeid);

	public abstract List<Work> getAllWorks();

}