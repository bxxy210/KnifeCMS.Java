package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Subscribe;

/**
 * 订阅接口
 * @author Knife
 *
 */
public interface SubscribeService {

	/**
	 * 取得订阅
	 * @return 订阅对象
	 */
	Subscribe getSubscribe();
	
	/**
	 * 取得所有订阅
	 * @return 订阅列表
	 */
	List<Subscribe> getAllSubscribes();
	
	/**
	 * 查询取得订阅
	 * @param sql 查询语句
	 * @return 订阅列表
	 */
	List<Subscribe> getSubscribesBySql(String sql);
	
	/**
	 * 查询取得订阅
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始位置
	 * @param end 结束位置
	 * @return 订阅列表
	 */
	List<Subscribe> getSubscribesBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按编号取得订阅
	 * @param id 编号
	 * @return 订阅对象
	 */
	Subscribe getSubscribeById(String id);
	
	/**
	 * 按邮件取得订阅
	 * @param mail 邮件
	 * @return 订阅对象
	 */
	Subscribe getSubscribeByMail(String mail);
	
	/**
	 * 保存订阅
	 * @param subs 订阅对象
	 * @return true保存成功,false保存失败
	 */
	boolean saveSubscribe(Subscribe subs);
	
	/**
	 * 更新订阅
	 * @param subs 订阅对象
	 * @return true更新成功,false更新失败
	 */
	boolean updateSubscribe(Subscribe subs);
	
	/**
	 * 删除订阅
	 * @param subs 订阅对象
	 * @return true删除成功,false删除失败
	 */
	boolean delSubscribe(Subscribe subs);
}
