package com.knife.news.logic.impl;

import com.knife.web.tools.IDAO;

public abstract class DAOSupportService {
	protected IDAO dao = com.knife.web.tools.EasyDBODAO.getInstance();

	public IDAO getDao() {
		return dao;
	}

	public void setDao(IDAO dao) {
		this.dao = dao;
	}
}
