package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.CustomService;
import com.knife.news.object.Custom;

public class CustomServiceImpl extends DAOSupportService implements CustomService {
	private static CustomServiceImpl customDao = new CustomServiceImpl();

	public static CustomServiceImpl getInstance() {
		return customDao;
	}

	public boolean saveCustom(Custom custom) {
		com.knife.news.model.Custom acustom=new com.knife.news.model.Custom();
		custom.setId(acustom.getId());
		acustom.setCustom(custom);
		return this.dao.save(acustom);
	}
	
	public boolean updateCustom(Custom custom) {
		com.knife.news.model.Custom acustom=new com.knife.news.model.Custom();
		acustom.setCustom(custom);
		return this.dao.update(acustom);
	}
	
	public Custom getCustomById(String id) {
		try{
			com.knife.news.model.Custom acustom = (com.knife.news.model.Custom) this.dao.get(com.knife.news.model.Custom.class, id);
			Custom custom = new Custom(acustom);
			return custom;
		}catch(Exception e){
			return null;
		}
	}
	
	public boolean delCustom(Custom custom) {
		com.knife.news.model.Custom acustom=new com.knife.news.model.Custom();
		acustom.setCustom(custom);
		return dao.del(acustom);
	}
	
	public List<Custom> getAllCustom() {
		return changeListType(this.dao.query(com.knife.news.model.Custom.class, "id!=''"));
		//return this.dao.query(News.class, "k_type=?", paras);
	}
	
	public List<Custom> getCustomBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Custom.class, sql, paras, begin, end));
	}
	
	public List<Custom> changeListType(List<Object> objs){
		List<Custom> results = new ArrayList<Custom>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Custom robj = (com.knife.news.model.Custom) newobj;
				Custom tobj =  new Custom(robj);
				results.add(tobj);
			}
		}
		return results;	
	}
}
