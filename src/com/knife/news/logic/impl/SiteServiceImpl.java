package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.SiteService;
import com.knife.news.object.Site;

public class SiteServiceImpl extends DAOSupportService implements SiteService {
	private static SiteServiceImpl sitedao = new SiteServiceImpl(); 

	public static SiteServiceImpl getInstance() {
		return sitedao;
	}

	public Site getSite(){
		String siteid = (String)this.dao.uniqueResult("select id from k_site order by length(k_order),k_order");
		if(siteid!=null){
		Site mysite = this.getSiteById(siteid);
			return mysite;
		}else{
			return null;
		}
	}
	
	public String saveSite(Site site) {
		com.knife.news.model.Site asite=new com.knife.news.model.Site();
		site.setId(asite.getId());
		asite.setSite(site);
		if(this.dao.save(asite)){
			return asite.getId();
		}else{
			return "";
		}
	}

	public boolean updateSite(Site site) {
		com.knife.news.model.Site asite=new com.knife.news.model.Site();
		asite.setSite(site);
		return this.dao.update(asite);
	}

	public boolean delSite(Site site) {
		com.knife.news.model.Site asite=new com.knife.news.model.Site();
		asite.setSite(site);
		return this.dao.del(asite);
	}
	
	public Site getSiteById(String siteid){
		//防止sql注入
		//List<com.knife.news.object.Site> sites=changeListType(this.dao.query(com.knife.news.model.Site.class, "id='"+siteid+"'"));
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(siteid);
		List<com.knife.news.object.Site> sites=this.getSitesBySql("id=?",paras,0,-1);
		if(sites!=null){
			if(sites.size()>0){
				Site site=sites.get(0);
				return site;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	public Site getSiteByTemplate(String tempalte){
		//防止sql注入
		//List<com.knife.news.object.Site> sites=changeListType(this.dao.query(com.knife.news.model.Site.class, "k_template='"+tempalte+"'"));
		Collection<Object> paras = new ArrayList<Object>();
		paras.add(tempalte);
		List<com.knife.news.object.Site> sites=this.getSitesBySql("k_template=?",paras,0,-1);
		if(sites!=null){
			if(sites.size()>0){
				Site site=sites.get(0);
				return site;
			}else{
				return null;
			}
		}else{
			return null;
		}
	}
	
	public List<Site> getAllSites(){
		return changeListType(dao.query(com.knife.news.model.Site.class, "id!='' order by length(k_order),k_order", null, 0, -1));
	}
	
	public List<Site> getSitesBySql(String sql) {
		return changeListType(this.dao.query(com.knife.news.model.Site.class, sql));
	}

	public List<Site> getSitesBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(com.knife.news.model.Site.class, sql, paras, begin, end));
	}
	
	public List<Site> changeListType(List<Object> objs){
		List<Site> results = new ArrayList<Site>();
		if(objs!=null){
			for (Object newobj : objs) {
				com.knife.news.model.Site robj = (com.knife.news.model.Site) newobj;
				Site tobj =  new Site(robj);
				results.add(tobj);
			}
		}
		return results;
	}
}