package com.knife.news.logic.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.knife.news.logic.FieldsService;
import com.knife.news.model.Fields;
import com.knife.news.object.FormField;

public class FieldsServiceImpl extends DAOSupportService implements FieldsService {
	private static FieldsServiceImpl fieldsdao = new FieldsServiceImpl();

	public static FieldsServiceImpl getInstance() {
		return fieldsdao;
	}

	public Fields getFields(){
		return (Fields)this.dao.uniqueResult("select * from k_fields");
	}
	
	public Fields getFieldsByNews(String newsid,String order){
		String sql="select id from k_fields where k_newsid='"+newsid+"' and k_order='"+order+"'";
		String fid = (String) this.dao.uniqueResult(sql);
		return this.getFieldsById(fid);
	}
	
	public List<Fields> getAllFields(){
		return changeListType(this.dao.query(Fields.class, "id!=''"));
	}
	
	public List<Fields> getFieldsBySql(String sql) {
		// TODO Auto-generated method stub
		return changeListType(this.dao.query(Fields.class, sql));
	}

	public List<Fields> getFieldsBySql(String sql, Collection<Object> paras, int begin, int end) {
		return changeListType(this.dao.query(Fields.class, sql, paras, begin, end));
	}
	
	public Fields getFieldsById(String id) {
		return	(Fields) this.dao.get(Fields.class, id);
	}
	
	public boolean saveFields(Fields fields) {
		return this.dao.save(fields);
	}

	public boolean updateFields(Fields fields) {
		return this.dao.update(fields);
	}

	public boolean delFields(Fields fields) {
		return this.dao.del(fields);
	}
	
    public List<FormField> jsonStringToList(String newsid,String json){
    	List<FormField> ret=new ArrayList<FormField>();
		String[] jsons= json.split("[|]");
		//System.out.println("待处理的数据:"+json);
		for(int j=0;j<jsons.length;j++){
			//System.out.println("运行到了这里"+jsons[j]);
			if(jsons[j].length()>1){
			String ajson =jsons[j].substring(1,jsons[j].length()-1);
			String[] values = ajson.split(",");
			FormField formField=new FormField();
			for(int k=0;k<values.length;k++){
				String aValue = values[k].substring(values[k].indexOf(":")+1);
				if(aValue.length()>2){
					aValue = aValue.substring(1,aValue.length()-1);
				}else{
					aValue ="";
				}
				//System.out.println("获取到的json值是"+aValue);
				switch(k){
					case 0:
						formField.setName(aValue);
						break;
					case 1:
						formField.setLabel(aValue);
						break;
					case 2:
						formField.setOrder(aValue);
						break;
					case 3:
						formField.setType(aValue);
						break;
					case 4:
						formField.setValue(aValue.replaceAll("\n","\\\\n").replaceAll("\r",""));
						break;
					case 5:
						formField.setRequire(aValue);
						break;
				}
			}
			if(!newsid.equals("0")){
				Fields afield = fieldsdao.getFieldsByNews(newsid,formField.getOrder());
				if(afield!=null){
					formField.setUserValue(afield.getValue());
				}
			}
			//fieldsValue.add(afield.getValue());
			ret.add(formField);
			}
		}
		return ret;
	}
    
	public List<Fields> changeListType(List<Object> news){
		List<Fields> results = new ArrayList<Fields>();
		if(news!=null){
			for (Object newobj : news) {
				Fields bnews = (Fields) newobj;
				results.add(bnews);
			}
		}
		return results;	
	}
}