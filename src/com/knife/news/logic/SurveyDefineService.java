package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.SurveyDefine;

/**
 * 调查接口
 * @author Knife
 *
 */
public interface SurveyDefineService {

	/**
	 * 取得调查
	 * @return 调查对象
	 */
	SurveyDefine getSurveyDefine();
	
	/**
	 * 取得所有调查列表
	 * @return 调查列表
	 */
	List<SurveyDefine> getAllSurveyDefines();
	
	/**
	 * 按条件取得调查列表
	 * @param sql 查询语句
	 * @return 调查列表
	 */
	List<SurveyDefine> getSurveyDefinesBySql(String sql);
	
	/**
	 * 按条件取得指定范围内的调查列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 调查列表
	 */
	List<SurveyDefine> getSurveyDefinesBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按编号取得编号
	 * @param id 调查编号
	 * @return 调查对象
	 */
	SurveyDefine getSurveyDefineById(String id);
	
	/**
	 * 保存调查
	 * @param surveyDefine 调查对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveSurveyDefine(SurveyDefine surveyDefine);
	
	/**
	 * 更新调查
	 * @param surveyDefine 调查对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateSurveyDefine(SurveyDefine surveyDefine);
	
	/**
	 * 删除调查
	 * @param surveyDefine 调查对象
	 * @return true删除成功，false删除失败
	 */
	boolean delSurveyDefine(SurveyDefine surveyDefine);
}
