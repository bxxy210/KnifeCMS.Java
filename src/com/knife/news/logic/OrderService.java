package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Order;

/**
 * 订单接口
 * @author Knife
 *
 */
public interface OrderService {
	
	/**
	 * 取得所有订单列表
	 * @return 订单列表
	 */
	List<Order> getAllOrders();
	
	/**
	 * 取得分类下的订单列表
	 * @param typeid 分类编号
	 * @return 订单列表
	 */
	List<Order> getOrdersByType(String typeid);
	
	/**
	 * 取得分类树下的订单列表
	 * @param treeid 分类树编号
	 * @return 订单列表
	 */
	List<Order> getOrdersByTree(String treeid);
	
	/**
	 * 取得文章下的订单列表
	 * @param newsid 文章编号
	 * @return 订单列表
	 */
	List<Order> getOrdersByNewsId(String newsid);
	
	/**
	 * 取得文章下的订单列表
	 * @param newsid 文章编号
	 * @return 订单列表
	 */
	List<Order> getOrdersByNewsId(String newsid,String orderby);
	
	/**
	 * 按条件取得订单列表
	 * @param sql 查询条件
	 * @return 订单列表
	 */
	List<Order> getOrdersBySql(String sql);
	
	/**
	 * 按条件取得指定范围的订单列表
	 * @param sql 查询条件
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 订单列表
	 */
	List<Order> getOrdersBySql(String sql, Collection<Object> paras, int begin, int end);
	
	/**
	 * 按路径取得订单列表
	 * @param url 订单路径
	 * @return 订单列表
	 */
	List<Order> getOrdersByUrl(String url);
	
	/**
	 * 按编号取得订单
	 * @param id 订单编号
	 * @return 订单对象
	 */
	Order getOrderById(String id);
	
	/**
	 * 保存订单
	 * @param order 订单对象
	 * @return ture保存成功，false保存失败
	 */
	boolean saveOrder(Order order);
	
	/**
	 * 更新订单
	 * @param order 订单对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateOrder(Order order);
	
	/**
	 * 删除订单
	 * @param order 订单对象
	 * @return true删除成功，false删除失败
	 */
	boolean delOrder(Order order);
}
