package com.knife.news.logic;


import com.knife.news.object.NewsVersion;
import java.util.Collection;
import java.util.List;

public interface NewsVersionService {

	/**
	 * 添加文章版本
	 * 
	 * @param news
	 *            文章版本对象
	 * @return 新文章版本编号，若失败则返回""
	 */
	String addNewsVersion(NewsVersion newsv);
	
	/**
	 * 按编号取得文章版本
	 * 
	 * @param id
	 *            文章版本编号
	 * @return 文章版本对象
	 */
	NewsVersion getNewsVersionById(String id);
	
	/**
	 * 保存文章版本
	 * 
	 * @param news
	 *            文章版本对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveNewsVersion(NewsVersion newsv);

	/**
	 * 更新文章版本
	 * 
	 * @param news
	 *            文章版本对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateNewsVersion(NewsVersion newsv);

	/**
	 * 删除文章版本
	 * 
	 * @param news
	 *            文章版本对象
	 * @return true删除成功，false删除失败
	 */
	boolean delNewsVersion(NewsVersion newsv);
	
	/**
	 * 按文章编号取得文章版本列表
	 * 
	 * @param newsid
	 *            文章编号
	 * @return 文章版本列表
	 */
	List<NewsVersion> getNewsVersionByNewsid(String newsid);
	
	/**
	 * 按条件查询文章版本列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始序号
	 * @param end 结束序号
	 * @return 文章版本列表
	 */
	public List<NewsVersion> getNewsVersionBySql(String sql, Collection<Object> paras, int begin, int end);
}
