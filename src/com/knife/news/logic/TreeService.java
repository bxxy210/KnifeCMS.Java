package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Tree;

/**
 * 树接口
 * @author Knife
 *
 */
public interface TreeService {
	
	/**
	 * 保存树
	 * @param tree 树对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveTree(Tree tree);

	/**
	 * 删除树
	 * @param tree 树对象
	 * @return true删除成功，false删除失败
	 */
	boolean delTree(Tree tree);

	/**
	 * 更新树
	 * @param tree 树对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateTree(Tree tree);

	/**
	 * 根据编号取得树
	 * @param id 树编号
	 * @return 树对象
	 */
	Tree getTreeById(String id);
	
	/**
	 * 取得所有树
	 * @param isshow 是否显示，0不显示，1显示
	 * @return 树列表
	 */
	List<Tree> getAllTree(int isshow);
	
	/**
	 * 按条件查询树列表
	 * @param sql 查询语句
	 * @return 分类列表
	 */
	List<Tree> getTreesBySql(String sql);

	/**
	 * 按条件查询指定范围内的树列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 分类列表
	 */
	List<Tree> getTreesBySql(String sql,
			Collection<Object> paras, int begin, int end);
}