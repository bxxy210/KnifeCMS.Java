package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Type2;

/**
 * 二维分类接口
 * 
 * @author Knife
 * 
 */
public interface Type2Service {

	/**
	 * 保存分类
	 * @param type 二维分类对象
	 * @return true保存成功，false保存失败
	 */
	boolean saveType(Type2 type);

	/**
	 * 更新分类
	 * @param type 二维分类对象
	 * @return true更新成功，false更新失败
	 */
	boolean updateType(Type2 type);

	/**
	 * 删除分类
	 * @param type 二维分类对象
	 * @return true删除成功，false删除失败
	 */
	boolean delType(Type2 type);

	/**
	 * 取得所有分类列表
	 * @return 分类列表
	 */
	List<com.knife.news.object.Type2> getAllTypes();

	/**
	 * 按条件查询分类列表
	 * @param sql 查询语句
	 * @return 分类列表
	 */
	List<com.knife.news.object.Type2> getTypesBySql(String sql);

	/**
	 * 按条件查询指定范围内的分类列表
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始编号
	 * @param end 结束编号
	 * @return 分类列表
	 */
	List<com.knife.news.object.Type2> getTypesBySql(String sql,
			Collection<Object> paras, int begin, int end);

	/**
	 * 按分类编号取得分类
	 * @param id 分类编号
	 * @return 分类对象
	 */
	com.knife.news.object.Type2 getTypeById(String id);

	/**
	 * 按分类名称取得分类
	 * @param name 分类名称
	 * @return 分类对象
	 */
	com.knife.news.object.Type2 getTypeByName(String name);
	
	/**
	 * 取得分类总数
	 * @param display 是否显示
	 * @param parentid 父分类编号
	 * @param tree_id 树编号
	 * @return 分类数
	 */
	int getSum(int display,String parentid,String tree_id);
}
