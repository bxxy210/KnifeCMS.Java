package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.object.Custom;

/**
 * 任意文件接口
 * @author Knife
 *
 */
public interface CustomService {
	
	/**
	 * 保存日志
	 * @param custom
	 * @return true保存成功,false保存失败
	 */
	boolean saveCustom(Custom custom);
	
	/**
	 * 更新日志
	 * @param custom
	 * @return true更新成功,false更新失败
	 */
	boolean updateCustom(Custom custom);
	
	/**
	 * 按编号取得日志
	 * @param id 编号
	 * @return 日志对象
	 */
	Custom getCustomById(String id);
	
	/**
	 * 删除日志
	 * @param custom
	 * @return 日志对象
	 */
	boolean delCustom(Custom custom);
	
	/**
	 * 取得所有日志
	 * @return 日志列表
	 */
	List<Custom> getAllCustom();
	
	/**
	 * 按查询条件取得日志
	 * @param sql 查询语句
	 * @param paras 查询参数
	 * @param begin 起始地址
	 * @param end 结束地址
	 * @return 日志列表
	 */
	List<Custom> getCustomBySql(String sql, Collection<Object> paras, int begin, int end);
}
