package com.knife.news.logic;

import java.util.Collection;
import java.util.List;

import com.knife.news.model.Filter;

/**
 * 过滤器接口
 * @author Knife
 *
 */
public interface FiltService {
	
	boolean saveFilt(Filter filter);
	
	boolean updateFilt(Filter filter);
	
	Filter getFilterByName(String name);
	
	Filter getFilterById(String id);
	
	boolean delFilter(Filter f);
	
	List<Filter> getAllFilters();
	
	List<Filter> getFiltersBySql(String sql, Collection<Object> paras, int begin, int end);
}
