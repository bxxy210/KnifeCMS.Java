package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_theme", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Theme {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_name")
	private String name;

	@TableField(name = "k_description")
	private String description;
	
	@TableField(name = "k_css")
	private String css;
	
	@TableField(name = "k_status")
	private int status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setTheme(com.knife.news.object.Theme theme){
		this.id			= theme.getId();
		this.name		= theme.getName();
		this.description= theme.getDescription();
		this.css		= theme.getCss();
		this.status		= theme.getStatus();
	}
}
