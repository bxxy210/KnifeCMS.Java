package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_news_version", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class NewsVersion {
	@TableField(name = "id")
	private String id;
	
	@TableField(name = "k_versionnumber")
	private int versionnumber;
	
	@TableField(name = "k_newsid")
	private String newsid;
	
	@TableField(name = "k_description")
	private String description;

	@TableField(name = "k_title")
	private String title;
	
	@TableField(name = "k_author")
	private String author;
	
	@TableField(name = "k_source")
	private String source;

	@TableField(name = "k_keywords")
	private String keywords;
	
	@TableField(name = "k_file")
	private String file;

	@TableField(name = "k_content")
	private String content;

	@TableField(name = "k_date")
	private Date date;

	@TableField(name = "k_username")
	private String username;

	@TableField(name = "k_type")
	private String type;
	
	@TableField(name = "k_treenews")
	private String treenews;

	@TableField(name = "k_display")
	private int display;
	
    @TableField(name="k_commend")
    private String commend;
    
    @TableField(name="k_template")
    private String template;
    
    @TableField(name="k_order")
    private int order;

    @TableField(name="k_link")
    private String link;  
    
    @TableField(name="k_tofront")
    private int	 tofront;
    
    @TableField(name="k_count")
    private int count;
    
    @TableField(name="k_docfile")
	private String docfile;
    
    @TableField(name="k_isfee")
	private int isfee;
    
    @TableField(name="k_rights")
	private int rights;
    
    @TableField(name="k_type2")
	private String type2;
    
    @TableField(name="k_doctype")
	private String doctype;
    
    @TableField(name="k_summary")
	private String summary;
    
    @TableField(name="k_score")
	private int score;
    
	@TableField(name = "k_search")
	private int search;
	
	@TableField(name = "k_ispub")
	private int ispub;
	
	@TableField(name = "k_offdate")
	private Date offdate;
	
	@TableField(name = "k_xmldata")
	private String xmldata;
	
	@TableField(name = "k_flowusers")
	private String flowusers;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	
	/**
	 * @return the versionnumber
	 */
	public int getVersionnumber() {
		return versionnumber;
	}

	/**
	 * @param versionnumber the versionnumber to set
	 */
	public void setVersionnumber(int versionnumber) {
		this.versionnumber = versionnumber;
	}

	/**
	 * @return the newsid
	 */
	public String getNewsid() {
		return newsid;
	}

	/**
	 * @param newsid the newsid to set
	 */
	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the treenews
	 */
	public String getTreenews() {
		return treenews;
	}

	/**
	 * @param treenews the treenews to set
	 */
	public void setTreenews(String treenews) {
		this.treenews = treenews;
	}

	/**
	 * @return the display
	 */
	public int getDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(int display) {
		this.display = display;
	}

	/**
	 * @return the commend
	 */
	public String getCommend() {
		return commend;
	}

	/**
	 * @param commend the commend to set
	 */
	public void setCommend(String commend) {
		this.commend = commend;
	}

	/**
	 * @return the template
	 */
	public String getTemplate() {
		return template;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(String template) {
		this.template = template;
	}

	/**
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(int order) {
		this.order = order;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the tofront
	 */
	public int getTofront() {
		return tofront;
	}

	/**
	 * @param tofront the tofront to set
	 */
	public void setTofront(int tofront) {
		this.tofront = tofront;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * @return the docfile
	 */
	public String getDocfile() {
		return docfile;
	}

	/**
	 * @param docfile the docfile to set
	 */
	public void setDocfile(String docfile) {
		this.docfile = docfile;
	}

	/**
	 * @return the isfee
	 */
	public int getIsfee() {
		return isfee;
	}

	/**
	 * @param isfee the isfee to set
	 */
	public void setIsfee(int isfee) {
		this.isfee = isfee;
	}

	/**
	 * @return the rights
	 */
	public int getRights() {
		return rights;
	}

	/**
	 * @param rights the rights to set
	 */
	public void setRights(int rights) {
		this.rights = rights;
	}

	/**
	 * @return the type2
	 */
	public String getType2() {
		return type2;
	}

	/**
	 * @param type2 the type2 to set
	 */
	public void setType2(String type2) {
		this.type2 = type2;
	}

	/**
	 * @return the doctype
	 */
	public String getDoctype() {
		return doctype;
	}

	/**
	 * @param doctype the doctype to set
	 */
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}

	/**
	 * @return the summary
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param summary the summary to set
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * @return the score
	 */
	public int getScore() {
		return score;
	}

	/**
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * @return the search
	 */
	public int getSearch() {
		return search;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(int search) {
		this.search = search;
	}

	/**
	 * @return the ispub
	 */
	public int getIspub() {
		return ispub;
	}

	/**
	 * @param ispub the ispub to set
	 */
	public void setIspub(int ispub) {
		this.ispub = ispub;
	}

	/**
	 * @return the offdate
	 */
	public Date getOffdate() {
		return offdate;
	}

	/**
	 * @param offdate the offdate to set
	 */
	public void setOffdate(Date offdate) {
		this.offdate = offdate;
	}

	/**
	 * @return the xmldata
	 */
	public String getXmldata() {
		return xmldata;
	}

	/**
	 * @param xmldata the xmldata to set
	 */
	public void setXmldata(String xmldata) {
		this.xmldata = xmldata;
	}

	/**
	 * @return the flowusers
	 */
	public String getFlowusers() {
		return flowusers;
	}

	/**
	 * @param flowusers the flowusers to set
	 */
	public void setFlowusers(String flowusers) {
		this.flowusers = flowusers;
	}
	
	public void setNewsVersion(com.knife.news.object.NewsVersion newsversion){
		this.id=newsversion.getId();
		this.newsid = newsversion.getNewsid();
		this.versionnumber = newsversion.getVersionnumber();
		this.description = newsversion.getDescription();
    	this.title=newsversion.getTitle();
    	this.author=newsversion.getAuthor();
    	this.source=newsversion.getSource();
    	this.keywords=newsversion.getKeywords();
    	this.file=newsversion.getFile();
    	this.content=newsversion.getContent();
    	this.date = newsversion.getDate();
    	this.username=newsversion.getUsername();
    	this.type=newsversion.getType();
    	this.treenews=newsversion.getTreenews();
    	this.display=newsversion.getDisplay();
    	this.commend=newsversion.getCommend();
    	this.template=newsversion.getTemplate();
    	this.order=newsversion.getOrder();
    	this.link=newsversion.getLink();
    	this.tofront = newsversion.getTofront();
    	this.count = newsversion.getCount();
    	this.docfile=newsversion.getDocfile();
    	this.isfee=newsversion.getIsfee();
    	this.rights=newsversion.getRights();
    	this.type2=newsversion.getType2();
    	this.doctype=newsversion.getDoctype();
    	this.summary=newsversion.getSummary();
    	this.score=newsversion.getScore();
    	this.search=newsversion.getSearch();
    	this.ispub=newsversion.getIspub();
    	this.offdate=newsversion.getOffdate();
    	this.xmldata=newsversion.getXmldata();
    	this.flowusers = newsversion.getFlowusers();
	}
	
}
