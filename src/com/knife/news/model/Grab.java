package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_grab", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Grab {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_title")
	private String title;
	
	@TableField(name = "k_url")
	private String url;
	
	@TableField(name = "k_date")
	private Date date;
	
	@TableField(name = "k_listdna_head")
	private String listdna_head;

	@TableField(name = "k_listdna_foot")
	private String listdna_foot;
	
	@TableField(name = "k_content_dna")
	private String content_dna;
	
	@TableField(name = "k_url2")
	private String url2;
	
	@TableField(name = "k_pagedna_head")
	private String pagedna_head;
	
	@TableField(name = "k_pagedna_foot")
	private String pagedna_foot;
	

	public String getPagedna_head() {
		return pagedna_head;
	}

	public void setPagedna_head(String pagedna_head) {
		this.pagedna_head = pagedna_head;
	}

	public String getPagedna_foot() {
		return pagedna_foot;
	}

	public void setPagedna_foot(String pagedna_foot) {
		this.pagedna_foot = pagedna_foot;
	}

	public String getUrl2() {
		return url2;
	}

	public void setUrl2(String url2) {
		this.url2 = url2;
	}

	public String getListdna_head() {
		return listdna_head;
	}

	public void setListdna_head(String listdna_head) {
		this.listdna_head = listdna_head;
	}

	public String getListdna_foot() {
		return listdna_foot;
	}

	public void setListdna_foot(String listdna_foot) {
		this.listdna_foot = listdna_foot;
	}

	public String getContent_dna() {
		return content_dna;
	}

	public void setContent_dna(String content_dna) {
		this.content_dna = content_dna;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setGrab(com.knife.news.object.Grab grab){
    	this.id=grab.getId();
    	this.title=grab.getTitle();
    	this.url=grab.getUrl();
    	this.date=grab.getDate();
    	this.content_dna=grab.getContent_dna();
    	this.listdna_head=grab.getListdna_head();
    	this.listdna_foot=grab.getListdna_foot();
    	this.url2 = grab.getUrl2();
    	this.pagedna_head = grab.getPagedna_head();
    	this.pagedna_foot = grab.getPagedna_foot();
	}
}
