package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName="k_filter",keyField="id",keyGenerator="com.knife.dbo.RandomIdGenerator")
public class Filter {
	@TableField(name="id")
	private String id;
	
    @TableField(name="k_name")
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
