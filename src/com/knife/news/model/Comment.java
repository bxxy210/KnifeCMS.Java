package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_comment", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Comment {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_user")
	private String user;
	//jlm
	@TableField(name = "k_uid")
	private Long uid;

	@TableField(name = "k_date")
	private Date date;

	@TableField(name = "k_comment")
	private String comment;

	@TableField(name = "k_news_id")
	private String news_id;
	
	@TableField(name = "k_display")
	private int display;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	//jlm
	public Long getUid(){
		return uid;
	}
	//jlm
	public void setUid(Long uid){
		 this.uid = uid;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNews_id() {
		return news_id;
	}

	public void setNews_id(String news_id) {
		this.news_id = news_id;
	}
	
	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public void setComment(com.knife.news.object.Comment comm){
		this.id=comm.getId();
		this.uid=comm.getUid();
		this.user=comm.getUser();
		this.date=comm.getDate();
		this.news_id=comm.getNews_id();
		this.comment=comm.getComment();
		this.display=comm.getDisplay();
	}
	//jlm
	public void addComment(com.knife.news.object.Comment comm){
		//this.id=comm.getId();
		this.uid=comm.getUid();
		this.user=comm.getUser();
		this.date=comm.getDate();
		this.news_id=comm.getNews_id();
		this.comment=comm.getComment();
		this.display=comm.getDisplay();
	}
	
}
