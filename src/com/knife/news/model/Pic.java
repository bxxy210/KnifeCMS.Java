package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_pic", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Pic {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_title")
	private String title;
	
	@TableField(name = "k_pic_type")
	private String pictype;

	@TableField(name = "k_url")
	private String url;
	
	@TableField(name = "k_newsid")
	private String newsid;

	@TableField(name = "k_userid")
	private int userid;
	
	@TableField(name = "k_date")
	private Date date;

	@TableField(name = "k_order")
	private int order;
	
	@TableField(name = "k_display")
	private int display;
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id=id;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title=title;
	}
	
	public String getPictype(){
		return pictype;
	}
	
	public void setPictype(String pictype){
		this.pictype=pictype;
	}
	
	public String getUrl(){
		return url;
	}
	
	public void setUrl(String url){
		this.url=url;
	}
	
	public String getNewsid(){
		return newsid;
	}
	
	public void setNewsid(String newsid){
		this.newsid=newsid;
	}
	
	public int getUserid(){
		return userid;
	}
	
	public void setUserid(int userid){
		this.userid=userid;
	}
	
	public Date getDate(){
		return date;
	}
	
	public void setDate(Date date){
		this.date=date;
	}
	
	public int getOrder(){
		return order;
	}
	
	public void setOrder(int order){
		this.order=order;
	}
	
	public int getDisplay() {
		return display;
	}

	public void setDisplay(int display) {
		this.display = display;
	}

	public void setPic(com.knife.news.object.Pic pic) {
		if(pic.getId()!=null){
			this.id		= pic.getId();
		}
		this.title	= pic.getTitle();
		this.pictype= pic.getPictype();
		this.url	= pic.getUrl();
		this.newsid	= pic.getNewsid();
		this.userid = pic.getUserid();
		this.date	= pic.getDate();
		this.order	= pic.getOrder();
		this.display= pic.getDisplay();
	}
}