package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_vote", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Vote {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_title")
	private String title;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
