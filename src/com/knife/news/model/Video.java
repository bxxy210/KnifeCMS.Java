package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_video", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Video {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_title")
	private String title;

	@TableField(name = "k_url")
	private String url;
	
	@TableField(name = "k_newsid")
	private String newsid;

	@TableField(name = "k_order")
	private int order;
	
	@TableField(name = "k_date")
	private Date date;
	
	@TableField(name = "k_userid")
	private int userid;
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id=id;
	}
	
	public String getTitle(){
		return title;
	}
	
	public void setTitle(String title){
		this.title=title;
	}
	
	public String getUrl(){
		return url;
	}
	
	public void setUrl(String url){
		this.url=url;
	}
	
	public String getNewsid(){
		return newsid;
	}
	
	public void setNewsid(String newsid){
		this.newsid=newsid;
	}
	
	public int getOrder(){
		return order;
	}
	
	public void setOrder(int order){
		this.order=order;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public void setVideo(com.knife.news.object.Video video) {
		this.id		= video.getId();
		this.title	= video.getTitle();
		this.url	= video.getUrl();
		this.newsid	= video.getNewsid();
		this.order	= video.getOrder();
		this.date 	= video.getDate();
		this.userid = video.getUserid();
	}
}