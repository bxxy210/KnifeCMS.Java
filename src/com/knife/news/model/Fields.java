package com.knife.news.model;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName="k_fields",keyField="id",keyGenerator="com.knife.dbo.RandomIdGenerator")
public class Fields {
	@TableField(name="id")
	private String id;
	
    @TableField(name="k_formid")
	private String formid;
    
    @TableField(name="k_name")
	private String name;
	
    @TableField(name="k_newsid")
	private String newsid;
    
    @TableField(name="k_order")
	private String order;
    
    @TableField(name="k_value")
	private String value;   

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFormid(){
		return formid;
	}
	
	public void setFormid(String formid){
		this.formid=formid;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public String getNewsid() {
		return newsid;
	}

	public void setNewsid(String newsid) {
		this.newsid = newsid;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
