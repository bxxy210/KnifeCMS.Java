package com.knife.news.model;

import java.util.Date;

import com.knife.dbo.annotation.Table;
import com.knife.dbo.annotation.TableField;

@Table(tableName = "k_order", keyField = "id", keyGenerator = "com.knife.dbo.RandomIdGenerator")
public class Order {
	@TableField(name = "id")
	private String id;

	@TableField(name = "k_user")
	private String user;
	
	@TableField(name = "k_title")
	private String title;

	@TableField(name = "k_date")
	private Date date;

	@TableField(name = "k_goods")
	private String goods;
	
	@TableField(name = "k_money")
	private int money;
	
	@TableField(name = "k_status")
	private int status;	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	public String getGoods() {
		return goods;
	}

	public void setGoods(String goods) {
		this.goods = goods;
	}
	
	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setOrder(com.knife.news.object.Order order){
		this.id=order.getId();
		this.user=order.getUser();
		this.title=order.getTitle();
		this.goods=order.getGoods();
		this.date=order.getDate();
		this.money=order.getMoney();
		this.status=order.getStatus();
	}
}
