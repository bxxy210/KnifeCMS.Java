package com.knife.member.stat;

import java.sql.Time;
import java.util.Date;

public class UserView {
	private int id;
	private String usermail;
	private String ip;
	private String os;
	private String browser;
	private String url;
	private Date accessdate;
	private Time accesstime;
	private Time endtime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsermail() {
		return usermail;
	}
	public void setUsermail(String usermail) {
		this.usermail = usermail;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Date getAccessdate() {
		return accessdate;
	}
	public void setAccessdate(Date accessdate) {
		this.accessdate = accessdate;
	}
	public Time getAccesstime() {
		return accesstime;
	}
	public void setAccesstime(Time accesstime) {
		this.accesstime = accesstime;
	}
	public Time getEndtime() {
		return endtime;
	}
	public void setEndtime(Time endtime) {
		this.endtime = endtime;
	}
}
