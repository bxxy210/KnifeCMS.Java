package com.knife.member.stat;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * 字符编码filter
 * <p>Title: </p>
 * <p>Description: </p>
 */
public class EncodingFilter extends HttpServlet implements Filter{
    /**
	 * 
	 */
	private static final long serialVersionUID = -5714591890536684274L;
	private String encoding = null;
    protected boolean ignore = true;

    public void init(FilterConfig filterConfig) throws ServletException{
        this.encoding = filterConfig.getInitParameter("encoding");
        String value = filterConfig.getInitParameter("ignore");
        if (value == null){
            this.ignore = true;
        } else if (value.equalsIgnoreCase("true")) {
            this.ignore = true;
        } else if (value.equalsIgnoreCase("yes")) {
            this.ignore = true;
        } else {
            this.ignore = false;
        }
    }

    //Process the request/response pair
    public void doFilter(ServletRequest request,ServletResponse response,
                         FilterChain filterChain){
        try{
            if (ignore || (request.getCharacterEncoding() == null)) {
                 String localencodeing = selectEncoding(request);
                 if (localencodeing != null)
                     request.setCharacterEncoding(localencodeing);
             }
            filterChain.doFilter(request,response);
        } catch(Exception sx){
            sx.getMessage();
        }

    }

    private String selectEncoding(ServletRequest request) {
        return (this.encoding);
    }

    //Clean up resources
    public void destroy(){
        encoding = null;
    }
}
