package com.knife.member;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Userfav entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.knife.member.Userfav
 * @author MyEclipse Persistence Tools
 */

public class UserfavDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(UserfavDAO.class);
	// property constants
	public static final String USERID = "userid";
	public static final String DOCID = "docid";
	public static final String TYPE = "type";
	public static final String FAVDATE = "favdate";

	public void save(Userfav transientInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void delete(Userfav persistentInstance) {

		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public Userfav findById(java.lang.Integer id) {

		try {
			Userfav instance = (Userfav) getSession().get("com.knife.member.Userfav", id);
			return instance;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByExample(Userfav instance) {

		try {
			List results = getSession().createCriteria("com.knife.member.Userfav").add(
					Example.create(instance)).list();

			return results;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {

		try {
			String queryString = "from Userfav as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List findByUserid(Object userid) {
		return findByProperty(USERID, userid);
	}

	public List findByDocid(Object docid) {
		return findByProperty(DOCID, docid);
	}
	
	public List findByType(int type) {
		return findByProperty(TYPE, type);
	}
	
	public List findByFavdate(Object favdate) {
		return findByProperty(FAVDATE, favdate);
	}

	public List findAll() {
		try {
			String queryString = "from Userfav";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {

			throw re;
		}
	}
	
	public List findAll(int page) {
		if(page<1){page=1;}
		try {
			String queryString = "from Userfav";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((page-1)*20);
			queryObject.setMaxResults(20);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List findByUseridAndDocid(int userid,String docid) {
		log.debug("finding all Userfav instances");
		try {
			String queryString = "from Userfav where userid=? and docid=?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, userid);
			queryObject.setParameter(1, docid);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
	public List findByUseridAndType(int userid,int type,int page) {
		log.debug("finding users Userfav instances page");
		//int page=1;
		try {
			//page=Integer.parseInt(apage.toString());
			//type=Integer.parseInt(atype.toString());
			if(page<1){page=1;}
			String queryString = "from Userfav where userid=? and type=? order by id desc";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, userid);
			queryObject.setParameter(1, type);
			queryObject.setFirstResult((page-1)*20);
			queryObject.setMaxResults(20);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}
	
	public List findByUseridAndType(Object userid,Object type) {
		log.debug("finding users Userfav instances");
		try {
			String queryString = "from Userfav where userid=? and type=?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, userid);
			if(type==null){
				queryObject.setParameter(1, 0);
			}else{
				queryObject.setParameter(1, type);
			}
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Userfav merge(Userfav detachedInstance) {

		try {
			Userfav result = (Userfav) getSession().merge(detachedInstance);
			return result;
		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachDirty(Userfav instance) {

		try {
			getSession().saveOrUpdate(instance);

		} catch (RuntimeException re) {

			throw re;
		}
	}

	public void attachClean(Userfav instance) {

		try {
			getSession().lock(instance, LockMode.NONE);

		} catch (RuntimeException re) {

			throw re;
		}
	}
}