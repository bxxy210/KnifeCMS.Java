package com.knife.member;

/**
 * Process entity. @author MyEclipse Persistence Tools
 */

public class RoomProc implements java.io.Serializable {

	// Fields

	private Integer id;
	private String contractguid;
	private String servicepro;
	// Constructors

	/** default constructor */
	public RoomProc() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getContractguid() {
		return contractguid;
	}

	public void setContractguid(String contractguid) {
		this.contractguid = contractguid;
	}

	public String getServicepro() {
		return servicepro;
	}

	public void setServicepro(String servicepro) {
		this.servicepro = servicepro;
	}

	
}