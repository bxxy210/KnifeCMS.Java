package com.knife.member;

import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A data access object (DAO) providing persistence and search support for
 * Meeting entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.knife.member.Meeting
 * @author MyEclipse Persistence Tools
 */

public class MeetingDAO extends BaseHibernateDAO {
	private static final Logger log = LoggerFactory.getLogger(MeetingDAO.class);
	// property constants
	public static final String TITLE = "title";
	public static final String HOST = "host";
	public static final String ISCLOSED = "isclosed";

	public void save(Meeting transientInstance) {
		log.debug("saving Meeting instance");
		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Meeting persistentInstance) {
		log.debug("deleting Meeting instance");
		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Meeting findById(java.lang.Integer id) {
		log.debug("getting Meeting instance with id: " + id);
		try {
			Meeting instance = (Meeting) getSession().get("com.knife.member.Meeting", id);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(Meeting instance) {
		log.debug("finding Meeting instance by example");
		try {
			List results = getSession().createCriteria("com.knife.member.Meeting").add(
					Example.create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Meeting instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Meeting as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByTitle(Object title) {
		return findByProperty(TITLE, title);
	}

	public List findByHost(Object host) {
		return findByProperty(HOST, host);
	}

	public List findByIsclosed(Object isclosed) {
		return findByProperty(ISCLOSED, isclosed);
	}

	public List findAll() {
		log.debug("finding all Meeting instances");
		try {
			String queryString = "from Meeting";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public int findAllSize() {

		return findAll().size();
	}

	public int getTotalPage(int size) {
		return (int) Math.ceil(findAllSize() / (size * 1.0));
	}

	public List findPagedAll(int currentPage, int pageSize) throws Exception {

		try {
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Meeting";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			return queryObject.list();
		} catch (Exception re) {

			throw re;
		}
	}

	public Meeting merge(Meeting detachedInstance) {
		log.debug("merging Meeting instance");
		try {
			Meeting result = (Meeting) getSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Meeting instance) {
		log.debug("attaching dirty Meeting instance");
		try {
			getSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Meeting instance) {
		log.debug("attaching clean Meeting instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}
}