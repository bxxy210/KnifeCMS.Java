package com.knife.member;

import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.criterion.Example;

/**
 * A data access object (DAO) providing persistence and search support for
 * Userinfo entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.knife.member.Userinfo
 * @author MyEclipse Persistence Tools
 */

public class UserinfoDAO extends BaseHibernateDAO {
	
	// property constants
	public static final String ID = "uid";
	public static final String IDCARD = "idcard";
	public static final String ACOUNT = "acount";
	public static final String PASSWORD = "password";
	public static final String REALNAME = "realname";
	public static final String SEX = "sex";
	public static final String EDUCATION = "education";
	public static final String PROFESSIONAL = "professional";
	public static final String EMAIL = "email";
	public static final String PHONE = "phone";
	public static final String MOBILE = "mobile";
	public static final String ORGANIZATION = "organization";
	public static final String DEPARTMENT = "department";
	public static final String POSITION = "position";
	public static final String LOCATION = "location";
	public static final String ZIPCODE = "zipcode";
	public static final String ISFEE = "isfee";
	public static final String ISADMIN = "isadmin";
	public static final String ADMINNUM = "adminnum";
	public static final String USEDNUM = "usednum";
	public static final String SCORE = "score";
	public static final String MAC = "mac";
	public static final String ACTIVE = "active";
	public static final String AVATAR = "avatar";
	public static final String COUNT = "count";

	public void save(Userinfo transientInstance) {
		try {
			getSession().getTransaction().begin();
			getSession().save(transientInstance);
			getSession().getTransaction().commit();
			
		} catch (Exception re) {
			
			re.printStackTrace();
		}
	}
	
	public void update(Userinfo transientInstance) {
		try {
			getSession().getTransaction().begin();
			getSession().update(transientInstance);
			getSession().getTransaction().commit();
			
		} catch (RuntimeException re) {
		
			throw re;
		}
	}

	public void delete(Userinfo persistentInstance) {
		try {
			getSession().getTransaction().begin();
			getSession().delete(persistentInstance);
			getSession().getTransaction().commit();
			
		} catch (RuntimeException re) {
			
			throw re;
		}
	}

	public Userinfo findById(java.lang.Long id) {
		try {
			getSession().clear();
			Userinfo instance = (Userinfo) getSession().get("com.knife.member.Userinfo", id);
			return instance;
		} catch (RuntimeException re) {
			
			throw re;
		}
	}
	//jlm
	public Userinfo findByUid(String uid) {
		Userinfo user=null;
		try {
			int userid=Integer.parseInt(uid);
			String queryString = "from Userinfo where id=?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, (long)userid);
			user=(Userinfo)queryObject.list().get(0);
		} catch (Exception re) {
			re.printStackTrace();
		}
		return user;
	}

	public List findByExample(Userinfo instance) {
		try {
			List results = getSession().createCriteria("com.knife.member.Userinfo").add(
					Example.create(instance)).list();
			
			return results;
		} catch (RuntimeException re) {
			
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		try {
			String queryString = "from Userinfo as model where model."
					+ propertyName + "= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			
			throw re;
		}
	}
	//jlm
//	public List findByUid(Object uid) {
//		return findByProperty(ID, uid);
//	}
//	
	public List findByIdcard(Object idcard) {
		return findByProperty(IDCARD, idcard);
	}

	public List findByAcount(Object acount) {
		return findByProperty(ACOUNT, acount);
	}

	public List findByPassword(Object password) {
		return findByProperty(PASSWORD, password);
	}

	public List findByRealname(Object realname) {
		return findByProperty(REALNAME, realname);
	}

	public List findBySex(Object sex) {
		return findByProperty(SEX, sex);
	}

	public List findByEducation(Object education) {
		return findByProperty(EDUCATION, education);
	}

	public List findByProfessional(Object professional) {
		return findByProperty(PROFESSIONAL, professional);
	}

	public List findByEmail(Object email) {
		getSession().clear();
		return findByProperty(EMAIL, email);
	}

	public List findByPhone(Object phone) {
		return findByProperty(PHONE, phone);
	}

	public List findByMobile(Object mobile) {
		return findByProperty(MOBILE, mobile);
	}

	public List findByOrganization(Object organization) {
		return findByProperty(ORGANIZATION, organization);
	}

	public List findByDepartment(Object department) {
		return findByProperty(DEPARTMENT, department);
	}

	public List findByPosition(Object position) {
		return findByProperty(POSITION, position);
	}

	public List findByLocation(Object location) {
		return findByProperty(LOCATION, location);
	}

	public List findByZipcode(Object zipcode) {
		return findByProperty(ZIPCODE, zipcode);
	}

	public List findByIsfee(Object isfee) {
		return findByProperty(ISFEE, isfee);
	}
	
	public List findByIsadmin(Object isadmin) {
		return findByProperty(ISADMIN, isadmin);
	}
	
	public List findByAdminnum(Object adminnum) {
		return findByProperty(ADMINNUM, adminnum);
	}
	
	public List findByUsednum(Object usednum) {
		return findByProperty(USEDNUM, usednum);
	}
	
	public List findByScore(Object score) {
		return findByProperty(SCORE, score);
	}
	
	public List findByMac(Object mac) {
		return findByProperty(MAC, mac);
	}
	
	public List findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}
	
	public List findByAvatar(Object avatar) {
		return findByProperty(AVATAR, avatar);
	}
	
	public List findActiveByAcount(Object acount) {
		try {
			getSession().clear();
			String queryString = "from Userinfo where acount=? and active=?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, acount);
			queryObject.setParameter(1, 1);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List findActiveByEmail(Object email) {
		try {
			getSession().clear();
			String queryString = "from Userinfo where email=? and active=?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, email);
			queryObject.setParameter(1, 1);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List findAll() {
		try {
			getSession().clear();
			String queryString = "from Userinfo";
			Query queryObject = getSession().createQuery(queryString);
			getSession().flush();
			getSession().clear();
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List findAllisfee(int isfee) {
		try {
			getSession().clear();
			String queryString = "from Userinfo where isfee= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, isfee);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List findPagedAllisfee(int currentPage, int pageSize, int isfee)
	throws Exception {
		try {
			getSession().clear();
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Userinfo where active>0 and isfee= ? ";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			queryObject.setParameter(0, isfee);
			return queryObject.list();
		} catch (Exception re) {
			throw re;
		}
	}
	public List findPagedisfee(int currentPage, int pageSize, int isfee)
	throws Exception {
		try {
			getSession().clear();
			if (currentPage < 1) {
				return null;
			}
		
				String queryString = "from Userinfo where active>0 and isfee>0";
			
			
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			//queryObject.setParameter(0, isfee);
			return queryObject.list();
		} catch (Exception re) {
		
			throw re;
		}
	}
	
	public List findAllNew() {
		try {
			getSession().clear();
			String queryString = "from Userinfo where acount is null or acount=''";
			Query queryObject = getSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List findPagedAllNew(int currentPage, int pageSize)
	throws Exception {
		try {
			getSession().clear();
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Userinfo where acount is null or acount='' order by id desc";
			//String queryString = "from Userinfo where idcard is null or idcard='' order by id desc";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			return queryObject.list();
		} catch (Exception re) {
			throw re;
		}
	}
	
	public Userinfo getDepartmentAdmin(String department){
		try {
			getSession().clear();
			String queryString = "from Userinfo where department= ? and isadmin=10";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, department);
			if(queryObject.list().size()>0){
				return (Userinfo)queryObject.list().get(0);
			}else{
				return null;
			}
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	public List findAllDepartment(String department) {
		try {
			getSession().clear();
			String queryString = "from Userinfo where department= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setParameter(0, department);
			return queryObject.list();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	public List findPagedAllDepartment(int currentPage, int pageSize, String department)
	throws Exception {
		try {
			getSession().clear();
			if (currentPage < 1) {
				return null;
			}
			String queryString = "from Userinfo where department= ?";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			queryObject.setParameter(0, department);
			return queryObject.list();
		} catch (Exception re) {
			throw re;
		}
	}
	
	public Userinfo merge(Userinfo detachedInstance) {
		
		try {
			Userinfo result = (Userinfo) getSession().merge(detachedInstance);
		
			return result;
		} catch (RuntimeException re) {
			
			throw re;
		}
	}

	public void attachDirty(Userinfo instance) {
		
		try {
			getSession().saveOrUpdate(instance);
			
		} catch (RuntimeException re) {
			
			throw re;
		}
	}

	public void attachClean(Userinfo instance) {	
		try {
			getSession().lock(instance, LockMode.NONE);
		
		} catch (RuntimeException re) {
		
			throw re;
		}
	}
	
	public List findPagedAll(int currentPage, int pageSize) throws Exception {
		try {
			if(getSession()!=null ){
			getSession().clear();
			}
			if (currentPage  < 1) {
				return null;
			}
			String queryString = "from Userinfo where active=1 order by id desc";
			Query queryObject = getSession().createQuery(queryString);
			queryObject.setFirstResult((currentPage - 1) * pageSize);
			queryObject.setMaxResults(pageSize);
			return queryObject.list();
		} catch (Exception re) {
			throw re;
		}
	}

	public int getTotalPage(int size)
	{
		String queryString="select count(*) from Userinfo";
		Query queryObject = getSession().createQuery(queryString);
		List list=queryObject.list();
		long count=Integer.parseInt(list.get(0).toString());
		return (int) Math.ceil(count/(size*1.0));
	}
	
	public int getTotalPageIsFee(int size,int isfee)
	{
		String queryString="select count(*) from Userinfo where active>0 and isfee=? order by id desc";
		Query queryObject = getSession().createQuery(queryString);
		queryObject.setParameter(0, isfee);
		List list=queryObject.list();
		long count=Integer.parseInt(list.get(0).toString());
		return (int) Math.ceil(count/(size*1.0));
	}
	
	public int getPageIsFee(int size,int isfee)
	{
		String queryString="select count(*) from Userinfo where active>0 and isfee>0 order by id desc";
		Query queryObject = getSession().createQuery(queryString);
		//queryObject.setParameter(0, isfee);
		List list=queryObject.list();
		long count=Integer.parseInt(list.get(0).toString());
		return (int) Math.ceil(count/(size*1.0));
	}
	
	public int getTotalPageNew(int size)
	{
		String queryString = "select count(*) from Userinfo where acount is null or acount=''";
		Query queryObject = getSession().createQuery(queryString);
		List list=queryObject.list();
		long count=Integer.parseInt(list.get(0).toString());
		return (int) Math.ceil(count/(size*1.0));
	}
	
	public int getTotalPageDepartment(int size,String department)
	{
		String queryString="select count(*) from Userinfo where department=?";
		Query queryObject = getSession().createQuery(queryString);
		queryObject.setParameter(0, department);
		List list=queryObject.list();
		long count=Integer.parseInt(list.get(0).toString());
		return (int) Math.ceil(findAllDepartment(department).size()/(size*1.0));
	}

}