package com.knife.member;

import java.sql.Timestamp;

/**
 * Document entity. @author MyEclipse Persistence Tools
 */

public class Document implements java.io.Serializable {

	private static final long serialVersionUID = -757911825450392630L;
	private Integer id;
	private String title;
	private String source;
	private String author;
	private String risktype;
	private String documenttype;
	private Timestamp date;
	private String filetype;
	private String fileurl;
	private Boolean isfee;

	// Constructors

	/** default constructor */
	public Document() {
	}

	/** minimal constructor */
	public Document(Timestamp date, Boolean isfee) {
		this.date = date;
		this.isfee = isfee;
	}

	/** full constructor */
	public Document(String title, String source, String author,
			String risktype, String documenttype, Timestamp date, String filetype,
			String fileurl, Boolean isfee) {
		this.title = title;
		this.source = source;
		this.author = author;
		this.risktype = risktype;
		this.documenttype = documenttype;
		this.date = date;
		this.filetype = filetype;
		this.fileurl = fileurl;
		this.isfee = isfee;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getRisktype() {
		return this.risktype;
	}

	public void setRisktype(String risktype) {
		this.risktype = risktype;
	}
	public String getDocumenttype() {
		return this.documenttype;
	}


	public void setDocumenttype(String documenttype) {
		this.documenttype =  documenttype;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getFiletype() {
		return this.filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	public String getFileurl() {
		return this.fileurl;
	}

	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}

	public Boolean getIsfee() {
		return this.isfee;
	}

	public void setIsfee(Boolean isfee) {
		this.isfee = isfee;
	}

}