package com.knife.service;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * KnifeCMS Publish Service Start
 * @author Smith
 *
 */

public class WebListener implements ServletContextListener{
    private static Timer timer;
    
    public void contextInitialized(ServletContextEvent event) {
        timer=new Timer();
        event.getServletContext().log("KnifeCMS:定时器已启动");
        //0表示任务无延迟,5*1000表示每隔5秒执行任务,60*60*1000表示一个小时
        //每隔5分钟执行一次
        timer.schedule(PublishTask.getInstance(), 1000, 1*30*1000);
    }
    
    public void contextDestroyed(ServletContextEvent event) {
        //在这里关闭监听器,同时关闭定时器
        timer.cancel();
        event.getServletContext().log("KnifeCMS:定时器关闭");
    }
}
