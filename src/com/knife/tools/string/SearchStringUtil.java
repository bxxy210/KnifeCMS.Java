package com.knife.tools.string;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
* @描述：这个java文件是为了寻找指定文件夹下的文件夹内以及子文件夹内的文件内 是否有某个特定的字符串
* @作者：（大和尚）smith
* @版本：1.1
*/
public class SearchStringUtil {
	List<String> results=new ArrayList<String>();

        public static void main(String agrs[]) {
                SearchStringUtil ftest = new SearchStringUtil();
                String[] fileType = { "asp", "txt", "php", "jsp", "css" };
                List<String> retest=ftest.findStr("sqlchkchar", fileType, "E:\\cms\\");// 这里修改目录路径
                System.out.println(retest.size());
        }

        /**
         * @描述：这个方法为了在指定的路径中的文件，子文件夹中找到str
         * @参数：String str: 要定位的str String[] fileType 要查找的文件的后缀列表 String pathStr
         *            指定的文件夹
         * @返回：List
         * @作者：（大和尚）
         */
        public List<String> findStr(String str, String[] fileType, String pathStr) {
        //        System.out.println("递归");
                File rootDir = new File(pathStr);
                if (!rootDir.exists()) {
                        //System.out.println("目录" + pathStr + "不存在");
                        return null;
                }
                if (!rootDir.isDirectory()) {
                        //System.out.println("文件" + pathStr + "不是目录！");
                        //System.out.println("请在第三个参数中务必输入一个目录的绝对路径，谢谢！！");
                        return null;
                }
                // else 即存在目录pathStr
                String[] dirlist = rootDir.list();
                int i1 = 0;
                
        /*        for(i1=0;i1<dirlist.length;i1++)
                {
                        System.out.println("文件有："+dirlist[i1]);
                }*/
                
                for (i1 = 0; i1 < dirlist.length; i1++) 
                {
                //        System.out.println(dirlist[i1]);
                        File chiFile = new File(pathStr + dirlist[i1]);
                        if (chiFile.isDirectory()) {// 是文件夹怎递归调用findStr函数
                                findStr(str, fileType, pathStr + dirlist[i1] + "\\");
                        } else 
                        {// 不是文件夹就是文件喽，是文件就打开到内存中查找是不是存在str，
                                // 存在str的话就输出文件路径
                                if (!chiFile.canRead()) {// 文件不能读
                                        continue;
                                }
                                // else 也就是 文件能读
                                int i2 = dirlist[i1].lastIndexOf('.');
                                String ftype = "";
                                if (i2 < 0)// 文件名中没有. 那么文件没有后缀
                                {
                                        // do nothing
                                        
                                } else {
                                        ftype = dirlist[i1].substring(i2+1); // 取其后缀
                                }

                                if (!isInStrList(ftype, fileType)) {// 文件类型不在要寻找的类型范围内
                                        //System.out.println(ftype+"不在检查范围内");
                                        continue;
                                }
                                // else 即文件类型正确

                                StringBuffer bstr = new StringBuffer(); // 用于保存文件内容
                                String tempstr;
                                try {
                                        FileReader fr = new FileReader(chiFile);
                                        BufferedReader br = new BufferedReader(fr);
                                        while (true) {
                                                tempstr = br.readLine();
                                                if (tempstr == null)
                                                {
                                                        break;
                                                }
                                                bstr.append(tempstr);
                                        }
                                        // bstr中保存了整个文件chiFile
                                        if (bstr.indexOf(str) > 0) {// chiFile文件中包含有要寻找的字符串
                                                System.out.println(chiFile.getAbsolutePath());
                                                results.add(chiFile.getName());
                                        }
                                        fr.close();
                                        br.close();
                                } catch (FileNotFoundException e) {
                                        // TODO Auto-generated catch block
                                        System.out.println("没有找到文件");
                                        e.printStackTrace();
                                } catch (IOException e) {
                                        // TODO Auto-generated catch block
                                        System.out.println("读文件时出错");
                                        e.printStackTrace();
                                }

                        }
                }
                return results;
        //        System.out.println("结束递归");
        }

        /**
         * @描述：判断一个字符串是否在一个字符串的数组中(不考虑大小写)
         * @参数：String str,要判断的字符串 String[] StrList,要判断是否包含str的字符串数组
         * @返回：boolean：如果StrList中包含Str那么就返回ture 如果StrList中不包含Str按么就返回false
         * @作者：(大和尚)
         * */
        public boolean isInStrList(String str, String[] StrList) {
                boolean temp = false;
                int lenoflist = StrList.length;
                for (int i = 0; i < lenoflist; i++) {
                        if (0 == str.compareToIgnoreCase(StrList[i])) {
                                return true;
                        }
                }
                return temp;
        }
}
