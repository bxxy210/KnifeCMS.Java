package com.knife.tools;

import info.monitorenter.cpdetector.io.ASCIIDetector;
import info.monitorenter.cpdetector.io.ByteOrderMarkDetector;
import info.monitorenter.cpdetector.io.CodepageDetectorProxy;
import info.monitorenter.cpdetector.io.JChardetFacade;
import info.monitorenter.cpdetector.io.ParsingDetector;

import java.io.File;

public class CheckCharset {
	private static CodepageDetectorProxy detector = CodepageDetectorProxy
			.getInstance();

	public static String checkFileEncoding(File document) {
		detector.add(new ByteOrderMarkDetector());
		// The first instance delegated to tries to detect the meta charset
		// attribut in html pages.
		detector.add(new ParsingDetector(true)); // be verbose about parsing.
		// This one does the tricks of exclusion and frequency detection, if
		// first implementation is
		// unsuccessful:
		detector.add(JChardetFacade.getInstance()); // Another singleton.
		detector.add(ASCIIDetector.getInstance()); // Fallback, see javadoc.
		String ret = "UTF-8";
		// Work with the configured proxy:
		java.nio.charset.Charset charset = null;
		try {
			charset = detector.detectCodepage(document.toURL());
		} catch (Exception e) {

		}
		if (charset != null) {
			ret = charset.name();
		}
		return ret;
	}
}
