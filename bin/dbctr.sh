#!/bin/sh
newdate=`date +%Y%m%d`

#主菜单
mainmenu(){
  clear
  echo "\n\n\n\t\t\t重庆农村商业银行门户系统数据库"
  thismachine=`echo |hostname`
  echo "\t本程序运行于:"`echo |host $thismachine`
  echo "\n"
  echo "\t1.迁移本机备份数据"
  echo "\t2.数据库备份"
  echo "\t3.数据库恢复"
  echo "\t4.数据库启动"
  echo "\t5.数据库停止"

  echo "\t\t0.退出"
  echo "\n"
  echo "\t\t请选择0-5:\c"
}

move_db_backup()
{
	echo "确定要迁移备份数据?[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		zip -r db_$newdate.zip /opt/backup/
		rsync -a root@10.181.76.59::log_tongbu db_$newdate.zip --password-file=/etc/rsync.pass
		rm -rf /opt/backup/*
		rm -rf db_$newdate.zip
		echo "\n迁移完成，请按任意键继续"
		read aaa
	fi
}

db_backup(){
	echo "确定要备份数据库?[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		pg_dump knifecms > /opt/backup/pg.dmp
		echo "\n备份完成，请按任意键继续"
		read aaa
	fi
}

db_restore(){
	echo "确定要恢复数据库?注意:恢复数据库会覆盖现有数据库[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		psql knifecms < /opt/backup/pg.dmp
		echo "\n恢复完成，请按任意键继续"
		read aaa
	fi
}

db_start(){
	service pgsql start
}

db_stop(){
	service pgsql stop
}

main()
{
	while [ 0 ]
	do
		mainmenu
		read input
		case $input in
			1) move_db_backup;;
			2) db_backup;;
			3) db_restore;;
			4) db_start;;
			5) db_stop;;
			0) clear
			exit 0;;
		esac
	done
}

#程序开始
#read_orgidt
main