#!/bin/sh
newdate=`date +%Y%m%d`

#主菜单
mainmenu(){
  clear
  echo "\n\n\n\t\t\t重庆农村商业银行门户系统维护"
  thismachine=`echo |hostname`
  echo "\t本程序运行于:"`echo |host $thismachine`
  echo "\n"
  echo "\t1.迁移本机应用日志"
  echo "\t2.迁移本机系统日志"
  echo "\t3.启动所有cms服务"
  echo "\t4.停止所有cms服务"

  echo "\t\t0.退出"
  echo "\n"
  echo "\t\t请选择0-4:\c"
}

move_app_logs()
{
	echo "确定要迁移应用日志?[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		zip -r app_logs_$newdate.zip /opt/logs/
		rsync -a root@10.181.76.59::log_tongbu app_logs_$newdate.zip --password-file=/etc/rsync.pass
		rm -rf /opt/logs/*
		rm -rf app_logs_$newdate.zip
		echo "\n迁移完成，请按任意键继续"
		read aaa
	fi
}

move_sys_logs()
{
	echo "确定要迁移系统日志?[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		zip -r sys_logs_$newdate.zip /opt/sys_logs/
		rsync -a root@10.181.76.59::log_tongbu sys_logs_$newdate.zip --password-file=/etc/rsync.pass
		rm -rf /opt/sys_logs/*
		rm -rf sys_logs_$newdate.zip
		echo "\n迁移完成，请按任意键继续"
		read aaa
	fi
}

cms_start(){
	echo "请确保在cms应用启动前已经处于停止状态?[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		/opt/tomcat/bin/startup.sh
		/usr/local/apache2/bin/apachectl start
		echo "\n启动完成，请按任意键继续"
		read aaa
	fi
}

cms_stop(){
	echo "确定要停止cms应用?[y/n]"
	read resp
	if [ $resp="y" -o $resp="Y"]
	then
		/opt/tomcat/bin/shutdown.sh
		/usr/local/apache2/bin/apachectl stop
		echo "\n应用停止，请按任意键继续"
		read aaa
	fi
}

main()
{
	while [ 0 ] 
	do
		mainmenu
		read input
		case $input in
			1) move_app_logs;;
			2) move_sys_logs;;
			3) cms_start;;
			4) cms_stop;;
			0) clear
			exit 0;;
		esac
	done
}

#程序开始
#read_orgidt
main