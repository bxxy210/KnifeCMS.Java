/*
	@ FinancialStreet ;
	Javascript Document;
	creat at 2011-05-08, creat by mypenn@gmail.com ;
*/
$(function(){
	SearchToggle("#SearchText");
	imgScroll("#imgScroll");
	
	var T2;
	//Navigation
	$("#Navigation>li").hover(function(){
		$(this).children("div").show();
	},function(){
		$(this).children("div").hide();
	});
	
	$(".news_list2 ul li:odd").addClass("bg");
	$(".cinfo ul li:even").addClass("bg");
	$(".ir3 table tbody tr:odd").addClass("bg");
	$(".hr_c table tbody tr:odd").addClass("bg");
	
	
	//IE6
	if($.browser.msie && parseInt($.browser.version,10) < 7){
		minHeight(".left",500);
	}
	
	$("img.new").each(function(){
		var o = $(this).parent().offset(),w = $(this).parent().width();
		$(this).css({
			top:o.top+5,
			left:o.left+w-5
		});
	});
	
	//
	$(".menu>ul>li").each(function(){
		var c = $(">a",this).attr("class");
		if(c.indexOf("active") != -1){
			$(">ul",this).show();
		}
		
		if($(">ul",this).length > 0){
			$(">a",this).addClass("hasm").click(function(){
				$(this).addClass("active").next("ul").slideDown(200).parent().siblings().children("a.active").removeClass("active").next("ul").slideUp(200);
				return false;
			});
		}
	});
})

//
function SearchToggle(obj){
	var text = $(obj).val();
	$(obj).focus(function(){
		($(this).val() == text) ? $(this).val("") : null;
	}).blur(function(){
		($(this).val() == "") ? $(this).val(text) : null;
	});
}

//imgScroll
function imgScroll(obj){
	var i = $("li",obj).length, w = $("li",obj).width(), m = 0, T;
	$("ul",obj).width(w * i);
	$(obj).append("<p></p>");
	for(var n=1;n<i+1;n++){
		$("p",obj).append("<a href='javascript:void(0)'>0"+n+"</a>");
	}
	$("p a",obj).hover(function(){
		m = $("p a",obj).index($(this));
		$(this).addClass("active").siblings().removeClass("active");
		$("ul",obj).animate({marginLeft:- w * m});
		clearTimeout(T);
	},function(){
		T = setTimeout(autoScroll,3000);
	}).click(function(){
		return false;
	});
	
	$("ul li img").hover(function(){
		clearTimeout(T);
	},function(){
		T = setTimeout(autoScroll,3000);
	});
	
	var autoScroll = function(){
		m < i-1 ? m++ : m = 0 ;
		$("p a",obj).eq(m).mouseover();
		T = setTimeout(autoScroll,3000);
	}
	$("p a",obj).eq(0).mouseover();
	T = setTimeout(autoScroll,3000);
	
	//
	imgScrollShow();
}

//
function minHeight(obj,height){
	if($(obj).height()<height){
		$(obj).height(height);
	}
}

//
function chageType(t,o){
	$(t).addClass("active").parent().siblings().children("a.active").removeClass("active");
	$(o).show().siblings().hide();
}

//
function tabToggle(t,o){
	$(t).addClass("active").parent().siblings().children("a.active").removeClass("active");
	$(o).show().siblings().hide();
}

//
//
function imgScrollShow(){
	var text ="",url = "",len = $("#imgList ul li").length,imgw = $("#imgList ul li").outerHeight(),i=0;
	/*
	$("#imgList ul li a img").css("opacity",0.75).hover(function(){
		$(this).css({"opacity":1});
	},function(){
		$(this).css({"opacity":0.75});
	});
	*/
	$("#imgList ul li a").click(function(){
		url = $(this).attr("href");
		text = $("img",this).attr("alt");
		i = $("#imgList ul li a").index($(this));
		$("#imgShow").empty().html("<img src='"+url+"' />");
		var h = $("#imgShow p.w").outerHeight();
		$("#imgShow img").hide().load(function(){
			$(this).fadeIn();
		});
		
		$("#ScrollPrev").click(function(){
			if(i==0){
				i=len-1;
			}else{
				i--;
			}
			$("#imgList ul li a").eq(i).click();
		});
		
		$("#ScrollNext").click(function(){
			if(i == len-1){
				i=0
			}else{
				i++;
			}
			$("#imgList ul li a").eq(i).click();
		});
		
		return false;
	});
	
	if(len > 4){
		$("#imgScroll1 a.scroll-next").click(function(){
			$("#imgList ul").animate({marginTop:-imgw},function(){
				$(this).append($("li:first",this)).css("margin-top",0);
			});
		});
		$("#imgScroll1 a.scroll-prev").click(function(){
			$("#imgList ul").prepend($("#imgList ul li:last")).css("margin-top",-imgw).animate({marginTop:0});
		});
		
		
	}
	
	$("#imgList ul li:first a").click();
}