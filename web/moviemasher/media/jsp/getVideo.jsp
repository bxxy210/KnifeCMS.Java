<%@ page language="java" pageEncoding="UTF-8" import="com.knife.news.model.Constants,com.knife.news.model.User,com.knife.tools.VideoConfig"%>
<%@ page import="java.io.*,java.net.URL,com.knife.web.Globals"%><%@ include file="/include/manage/checkUser.jsp" %><%
String video="";
if(request.getParameter("v")!=null){
	video = request.getParameter("v");
}
String vpath = Globals.APP_BASE_DIR + video;
vpath = vpath.replace("/",File.separator);
File f = new File(vpath);
if(!f.exists()){
	response.sendError(404,"File not found!");
	return;
}
BufferedInputStream br = new BufferedInputStream(new FileInputStream(f));
byte[] buf = new byte[1024];
int len = 0;

response.reset(); //非常重要
URL u = new URL("http://"+request.getServerName()+video);
response.setContentType(u.openConnection().getContentType());
response.setHeader("Content-Disposition", "inline; filename="+f.getName());
OutputStream outs = response.getOutputStream();
while((len = br.read(buf)) >0)
	outs.write(buf,0,len);
	//outs.write(len);
	br.close();
	outs.close();
%>