<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="inc/checkuser.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>修改密码</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	width:806px;
	font-size:12px;
	border-left:1px solid #BCC3CD;
}
.data_list th{
	height:30px;
	border-bottom:1px solid gray;
	color:white;
	background:url('/user/skin/images/document/title_bg.jpg') bottom repeat-x;
}
.data_list th div.title{
	position:relative;
	height:25px;
	_height:1%;
	line-height:25px;
}
.data_list td{
	height:19px;
	text-align:center;
	border-bottom:1px solid #BCC3CD;
	border-right:1px solid #BCC3CD;
}
.datatype{
	display:none;
	position:absolute;
	top:20px;
	right:0px;
	width:90px;
	line-height:150%!important;
	height:auto!important;
	_height:220px;
	max-height:220px;
	overflow-y:auto;
	border:1px solid gray;
	background:white;
	z-index:100;
}
.datatype ul{list-style-type:none;margin-left:0px;padding:0}
.datatype ul li{list-style-type:none;margin-left:0px;padding:0;text-align:center;}
.datatype ul li a{font-weight:normal}
#data_main{
	border-left:1px solid #BCC3CD;
	border-top:1px solid #BCC3CD;
}
#data_main td.inputText{text-align:left;padding:1px;}
</style>
	<link rel="stylesheet" href="../skin/js/validationEngine.jquery.css" type="text/css" />
	<script type="text/javascript" src="../skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="../skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="../skin/js/jquery.validationEngine-cn.js"></script>
	<script language="javascript">
	$(function(){
		$("#regform").validationEngine();
	});
	</script>
</head>
<body>
	<form id="regform" name="regform" action="/user/opt/editpassword.jsp" method="post">
	<input type="hidden" name="uid" value="<%=auser.getId()%>"/>
	<input type="hidden" name="acount" value="<%=auser.getAcount()%>"/>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>&nbsp;&nbsp;修改密码</th>
		</tr>
		<tr>
			<td>
				<table id="data_main" width="90%" style="margin:32px;" cellspacing="0" cellpadding="0" border="0" align="center">
				<tr>
					<td width="280">输入旧密码：</td>
					<td class="inputText">
						<input id="oldpassword" type="password" name="oldpassword" class="validate[required,length[6,6]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						新密码(至少6位)：
					</td>
					<td class="inputText">
						<input id="newpassword" type="password" name="newpassword" class="validate[required,length[6,6]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td>
						重复新密码：
					</td>
					<td class="inputText">
						<input id="newpassword2" type="password" name="newpassword2" class="validate[required,length[6,6],equals[newpassword]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td style="height:32px;text-align:center">
				<input name="submit" type="submit" value=" 提交 "/>&nbsp;&nbsp;&nbsp;&nbsp;
				<input name="reset" type="reset" value=" 重置 "/>
			</td>
		</tr>
	</table>
	</form>
</body>
</html>