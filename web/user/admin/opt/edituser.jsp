<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,java.sql.Timestamp" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
Boolean isreg=true;
int adduser=0;
UserinfoDAO userDAO = new UserinfoDAO();
Userinfo adminUser=null;
if(request.getParameter("adduser")!=null){
	adduser			= Integer.parseInt(request.getParameter("adduser"));
}
		//机构管理员
		if(adduser>0){
			adminUser = userDAO.findById((long)adduser);
			//已经使用完了所有名额
			if(adminUser.getUsednum()>=adminUser.getAdminnum()){
				msg="编辑失败！已经用完所有名额";
				isreg=false;
			}
		}

int	uid	=0;
if(request.getParameter("uid")!=null){
	uid				= Integer.parseInt(request.getParameter("uid"));
}
String acount 		= request.getParameter("acount");
String type			= request.getParameter("type");
String password 	= "123456";
String realname 	= request.getParameter("realname");
String sex			= request.getParameter("sex");
String birthday	= request.getParameter("birthday");
String sysj	= request.getParameter("sysj");

String country	= request.getParameter("country");
String education	= request.getParameter("education");
String idcard	= request.getParameter("idcard");
String homeaddress	= request.getParameter("homeaddress");
String homearea	= request.getParameter("homearea");
String workarea	= request.getParameter("workarea");
String family	= request.getParameter("family");
String childage	= request.getParameter("childage");
String traffictool	= request.getParameter("traffictool");
String department	= "";
if(request.getParameter("department")!=null){
	department		= request.getParameter("department");
}
String email		= request.getParameter("email");
String phone		= request.getParameter("phone");
String mobile		= request.getParameter("mobile");
String zipcode		= request.getParameter("zipcode");
String location		= request.getParameter("location");
String companynature	= request.getParameter("companynature");
String organization = request.getParameter("organization");
String salary	= request.getParameter("salary");
String hobby	= request.getParameter("hobby");
String buytarget	= request.getParameter("buytarget");
String buyuse	= request.getParameter("buyuse");
String payway	= request.getParameter("payway");
String selectreason	= request.getParameter("selectreason");
String buyagain	= request.getParameter("buyagain");
String attitude	= request.getParameter("attitude");
String joinreason	= request.getParameter("joinreason");
String professional = request.getParameter("professional");
int isfee		= 0;
if(request.getParameter("isfee")!=null){
	isfee		= Integer.parseInt(request.getParameter("isfee"));
}
int isadmin		= 0;
if(request.getParameter("isadmin")!=null){
	isadmin		= Integer.parseInt(request.getParameter("isadmin"));
}
int adminnum	= 0;
if(request.getParameter("adminnum")!=null){
	if(!"".equals(request.getParameter("adminnum").toString())){
		adminnum = Integer.parseInt(request.getParameter("adminnum"));
	}
}
String mac		= "";
if(request.getParameter("mac")!=null){
	mac			= request.getParameter("mac");
}
int active		= 0;
if(request.getParameter("active")!=null){
	active		= Integer.parseInt(request.getParameter("active"));
}
if(acount!=null && acount.equals("admin")){
	msg="编辑失败！不能使用关键字admin";
	isreg=false;
}
List checkList=userDAO.findByEmail(email);
if(checkList.size()>0){
	msg="编辑失败！邮箱已使用";
	isreg=false;
}
if(isreg){
	Userinfo newUser=new Userinfo();
	if(uid>0){
		newUser = userDAO.findById((long)uid);
	}
	newUser.setAcount(acount);
	newUser.setType(type);
	if(uid>0){
		//newUser.setPassword(password);
		//newUser.setEmail(email);
	}else{
		password=CheckPassword.generatePassword(password);
		newUser.setPassword(password);
		newUser.setEmail(email);
	}
	newUser.setRealname(realname);
	if(sex.equals("1")){
		newUser.setSex(true);
	}else{
		newUser.setSex(false);
	}
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
	try{ 
		Date applyDate = dateFormat.parse(birthday);
		Timestamp applyTime = new Timestamp(applyDate.getTime());
		newUser.setBirthday(applyTime);
	}catch(Exception e){
		//e.printStackTrace();
	}
	try{ 
		Date applyDate1 = dateFormat.parse(sysj);
		Timestamp applyTime1 = new Timestamp(applyDate1.getTime());
		newUser.setSysj(applyTime1);
	}catch(Exception e){
		//e.printStackTrace();
	}
	newUser.setEducation(education);
	newUser.setProfessional(professional);
	newUser.setPhone(phone);
	newUser.setMobile(mobile);
	if(department.length()>0){
		newUser.setDepartment(department);
	}
	newUser.setOrganization(organization);
	newUser.setLocation(location);
	newUser.setZipcode(zipcode);
	newUser.setIsfee(isfee);
	newUser.setIsadmin(isadmin);
	newUser.setAdminnum(adminnum);
	newUser.setMac(mac);
	newUser.setActive(active);
	newUser.setCountry(country);
	newUser.setIdcard(idcard);
	newUser.setHomeaddress(homeaddress);
	newUser.setHomearea(homearea);
	newUser.setWorkarea(workarea);
	newUser.setFamily(family);
	newUser.setChildage(childage);
	newUser.setTraffictool(traffictool);
	newUser.setDepartment(department);
	newUser.setCompanynature(companynature);
	newUser.setSalary(salary);
	newUser.setHobby(hobby);
	newUser.setBuytarget(buytarget);
	newUser.setBuyuse(buyuse);
	newUser.setPayway(payway);
	newUser.setSelectreason(selectreason);
	newUser.setBuyagain(buyagain);
	newUser.setAttitude(attitude);
	newUser.setJoinreason(joinreason);
	try{
		userDAO.save(newUser);
		if(adduser>0){
			if(adminUser!=null){
				UserinfoDAO adminDAO = new UserinfoDAO();
				adminUser.setId((long)adduser);
				adminUser.setUsednum(adminUser.getUsednum()+1);
				adminDAO.update(adminUser);
			}
		}
		msg="编辑成功！";
	}catch(Exception e){
		msg="编辑失败！";
	}
}
%>
<script language="javascript">
alert("<%=msg%>");
history.back(-1);
</script>