<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,com.knife.news.logic.NewsService,com.knife.news.logic.impl.NewsServiceImpl,com.knife.news.object.News" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
int begin = 1;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)page_now/5;
	page_next=page_now;
	begin = (page_now - 1) * 20 + 1;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>资料列表</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>资料名称</th>
			<th>来源</th>
			<th>作者</th>
			<th>风险类别</th>
			<th>资料类别</th>
			<th>收录时间</th>
			<th>类型</th>
			<th>操作</th>
		</tr>
		<%
		NewsService newsDAO=new NewsServiceImpl();
		List docs = newsDAO.getNewsBySql("id!='' and k_docfile!='' order by length(k_order) desc,k_order desc", null, begin - 1, 20);
		total_page = (int) Math.ceil((float) (docs.size()) / (float) (20));
		if(page_now>1)page_prev=page_now-1;
		if(page_now<total_page)page_next=page_now+1;
		
		for(Object aobj:docs){
		News adoc=(News)aobj;
		String source="";
		String author="";
		String riskName="";
		String doctName="";
		String fileType="";
		if(adoc.getType2()!=null){
			riskName=adoc.getType2();
		}
		if(adoc.getType()!=null){
			doctName=adoc.getType();
		}
		if(adoc.getSource()!=null){
			source=adoc.getSource();
		}
		if(adoc.getUsername()!=null){
			author=adoc.getUsername();
		}
		if(adoc.getDoctype()!=null){
			fileType=adoc.getDoctype();
		}
		%>
		<tr>
			<td><%=adoc.getTitle()%></td>
			<td><%=source%></td>
			<td><%=author%></td>
			<td><%=riskName%></td>
			<td><%=doctName%></td>
			<td><%=adoc.getDate()%></td>
			<td><%=fileType%></td>
			<td>
				<a href="/manage_news.ad?parameter=edit&id=<%=adoc.getId()%>">编辑</a> | 
				<a href="<%
			String urlType=adoc.getDocfile();
			if(urlType!=null){
				urlType=urlType.substring(urlType.lastIndexOf("."),urlType.length());
				if(urlType.equals(".swf")){
					out.print("dataview.jsp?url="+adoc.getDocfile());
				}else{
					out.print("videoview.jsp?url="+adoc.getDocfile());
				}
			}else{
				out.print("#");
			}
			%>">预览</a>
			</td>
		</tr>
		<%}%>
		<tr>
			<td colspan="8" style="height:32px" align="center">
				<a href="?page=1"><img src="/user/skin/images/page_first.gif" border="0" /></a>
				<a href="?page=<%=page_prev%>"><img src="/user/skin/images/page_prev.gif" border="0" /></a>
				<%for(int i=1+(page_range_start*5);i<5+(page_range_start*5);i++){
					if(i>total_page) break;
				%>
				<a href="?page=<%=i%>"><%=i%></a>
				<%
				}
				%>
				<a href="?page=<%=page_next%>"><img src="/user/skin/images/page_next.gif" border="0" /></a>
				<a href="?page=<%=total_page%>"><img src="/user/skin/images/page_last.gif" border="0" /></a>
			</td>
		</tr>
	</table>
</body>
</html>