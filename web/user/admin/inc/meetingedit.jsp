<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat" pageEncoding="UTF-8"%>
<%@include file="checkadmin.jsp" %>
<%
int	id = 0;
if(request.getParameter("id")!=null){
	id				= Integer.parseInt(request.getParameter("id"));
}
	MeetingDAO meetingDAO=new MeetingDAO();
	Meeting ameeting = meetingDAO.findById(id);
	String startTime="";
	String endTime="";
	SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm");
	try{
		startTime	= dateFormat.format(ameeting.getStarttime()); 
		endTime		= dateFormat.format(ameeting.getEndtime()); 
	}catch(Exception e){ 
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>编辑会议</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:24px;
	text-indent:24px;
	border-bottom:1px solid gray;
}
.inputText input{border:1px solid gray;}
</style>
	<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css" type="text/css"></link>
	<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine.js"></script>
	<script type="text/javascript" src="/user/skin/js/jquery.validationEngine-cn.js"></script>
	<script type="text/javascript" src="/user/datepicker/WdatePicker.js"></script>
	<script language="javascript">
	//jQuery.noConflict();
	$(function(){
		$("#regform").validationEngine();
	});
	
	function checkForm(){
		$("#regform")[0].submit();
	}
	
	function resetForm(){
		$("#regform")[0].reset();
	}
	</script>
  </head>
  
<body>
<form id="regform" name="regform" action="/user/opt/editmeeting.jsp" method="post">
<input type="hidden" name="id" value="<%=ameeting.getId()%>" />
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th colspan="2">编辑会议</th>
		</tr>
		<tr>
			<td width="280">会议名称：</td>
			<td class="inputText">
				<input id="title" type="text" name="title" class="validate[required,length[0,20]]" value="<%=ameeting.getTitle()%>" />
				<span class="mustInput">*</span>
			</td>
		</tr>
		<tr>
			<td width="280">主 持 人：</td>
			<td class="inputText">
				<input id="host" type="text" name="host" class="validate[required,length[0,20]]" value="<%=ameeting.getHost()%>" />
				<span class="mustInput">*</span>
			</td>
		</tr>
		<tr>
			<td width="280">会议编号：</td>
			<td class="inputText">
				<input id="meetingid" type="text" name="meetingid" class="validate[required,length[0,20]]" value="<%=ameeting.getMeetingid()%>" />诚迅通提供
				<span class="mustInput">*</span>
			</td>
		</tr>
		<tr>
			<td width="280">起始时间：</td>
			<td class="inputText">
				<input id="starttime" type="text" name="starttime" class="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" value="<%=startTime%>" />
			</td>
		</tr>
		<tr>
			<td width="280">结束时间：</td>
			<td class="inputText">
				<input id="endtime" type="text" name="endtime" class="Wdate" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm'})" value="<%=endTime%>" />
			</td>
		</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="提交" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="重置" />
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			</table>
	</form>
</body>
</html>