<%@ page language="java" import="java.util.*,com.knife.member.*,com.knife.member.stat.*" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
String url=request.getParameter("url");
session.setAttribute("permission","1");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>视频浏览</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
	height:485px;
	overflow:visible;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
        <script type="text/javascript" src="/user/reader/swfobject.js"></script>
        
<script type="text/javascript">
  var flashvars =
      {
        'file':                 '<%=url%>',
        'image':                '/player/preview.jpg',
        'id':                   'playerID',
        'autostart':            'true'
      };

      var params =
      {
        'allowfullscreen':      'true',
        'allowscriptaccess':    'always',
        'bgcolor':              '#000000'
      };

      var attributes =
      {
        'align':                'top',
        'name':                 'playerID',
        'id':                   'playerID'
      };

      swfobject.embedSWF('/user/player/KnifePlayer.swf', 'video', '100%', '483', '9.0.124', '/user/reader/playerProductInstall.swf', flashvars, params, attributes);
</script>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<td height="483" valign="top">
				<div id="video" style="height:483px;">
        	<p>
	        	To view this page ensure that Adobe Flash Player version 
				10.0.0 or greater is installed. 
			</p>
			<script type="text/javascript"> 
				var pageHost = ((document.location.protocol == "https:") ? "https://" :	"http://"); 
				document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" 
								+ pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player' /></a>" ); 
			</script>
        </div>
	   	
       	<noscript>
            <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="483" id="KnifeReader">
                <param name="movie" value="/user/player/KnifePlayer.swf" />
                <param name="quality" value="high" />
                <param name="bgcolor" value="#ffffff" />
                <param name="allowScriptAccess" value="sameDomain" />
                <param name="allowFullScreen" value="true" />
                <param name="flashvars" value="file=<%=url%>"/>
                <!--[if !IE]>-->
                <object type="application/x-shockwave-flash" data="/user/player/KnifePlayer.swf" width="100%" height="483">
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#ffffff" />
                    <param name="allowScriptAccess" value="sameDomain" />
                    <param name="allowFullScreen" value="true" />
                <!--<![endif]-->
                <!--[if gte IE 6]>-->
                	<p> 
                		Either scripts and active content are not permitted to run or Adobe Flash Player version
                		10.0.0 or greater is not installed.
                	</p>
                <!--<![endif]-->
                    <a href="http://www.adobe.com/go/getflashplayer">
                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash Player" />
                    </a>
                <!--[if !IE]>-->
                </object>
                <!--<![endif]-->
            </object>
	    </noscript>
			</td>
		</tr>
	</table>
</body>
</html>