<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp"%>
<%
int pid=Integer.parseInt(request.getParameter("pid").toString());
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>添加会员角色</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<style type="text/css">
body {
	margin: 0px;
	padding: 0px;
}

.data_list {
	border: 1px solid gray;
	width: 100%;
	font-size: 12px;
}

.data_list th {
	height: 32px;
	border-bottom: 1px solid gray;
}

.data_list td {
	height: 24px;
	text-indent: 24px;
	border-bottom: 1px solid gray;
}

.inputText input {
	border: 1px solid gray;
}
</style>
		<link rel="stylesheet" href="/user/skin/js/validationEngine.jquery.css"
			type="text/css"></link>
		<script type="text/javascript" src="/user/skin/js/jquery-1.6.4.min.js"></script>
		<script type="text/javascript"
			src="/user/skin/js/jquery.validationEngine.js"></script>
		<script type="text/javascript"
			src="/user/skin/js/jquery.validationEngine-cn.js"></script>
		<script language="javascript">
	$(function() {
		$("#regform").validationEngine();
	});

	function checkForm() {
		$("#regform")[0].submit();
	}

	function resetForm() {
		$("#regform")[0].reset();
	}
</script>
	</head>

	<body>
		<form id="regform" name="regform" action="edit_type.jsp"
			method="post">
			<input type="hidden" name="parent" value="<%=pid%>" />
			<table class="data_list" cellspacing="0" cellpadding="0">
				<tr>
					<th colspan="2">
						添加会员角色
					</th>
				</tr>
				<tr>
					<td width="280">
						角色名称：
					</td>
					<td class="inputText">
						<input id="type" type="text" name="name"
							class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td width="280">
						角色权限：
					</td>
					<td class="inputText">
						<input id="type" type="text" name="rights"
							class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td width="280">
						是否积分
					</td>
					<td class="inputText">
						<input type="radio" name="onscore" value="0" checked/>是&nbsp;
						<input type="radio" name="onscore" value="1"/>否
					</td>
				</tr>
				<tr>
					<td width="280">
						绑定网卡数：
					</td>
					<td class="inputText">
						<input id="type" type="text" name="macnum"
							class="validate[required,length[0,20]]" />
						<span class="mustInput">*</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" value="提交" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="reset" value="重置" />
					</td>
				</tr>
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>