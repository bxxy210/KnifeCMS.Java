<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat" pageEncoding="UTF-8"%>
<%@include file="checkadmin.jsp" %>
<%
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)(page_now/5);
	page_next=page_now;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>资料列表</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>资料名称</th>
			<th>来源</th>
			<th>作者</th>
			<th>风险类别</th>
			<th>资料类别</th>
			<th>收录时间</th>
			<th>类型</th>
			<th>操作</th>
		</tr>
		<%
			DocumentDAO dbdocDAO=new DocumentDAO();
				//这里需要提供一个带参数的方法
				//参数1.用户账号,2.页码,3.每页条数
				//List docs=dbdocDAO.findAll();
				List docs = dbdocDAO.findPagedAll(page_now,20);
				total_page = dbdocDAO.getTotalPage(20);
				if(page_now>1)page_prev=page_now-1;
				if(page_now<total_page)page_next=page_now+1;
				String applyDate="";
				for(Object aobj:docs){
				Document adoc=(Document)aobj;
				SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
				try{
			applyDate = dateFormat.format(adoc.getDate()); 
				}catch(Exception e){ 
			System.out.println("convert error....... ");
			//e.printStackTrace();
				}
		%>
		<tr>
			<td><%=adoc.getTitle()%></td>
			<td><%=adoc.getSource()%></td>
			<td><%=adoc.getAuthor()%></td>
			<td><%=adoc.getRisktype()%></td>
			<td><%=adoc.getDocumenttype()%></td>
			<td><%=applyDate%></td>
			<td><%=adoc.getFiletype()%></td>
			<td><a href="dataedit.jsp?id=<%=adoc.getId()%>">编辑</a></td>
		</tr>
		<%}%>
		<tr>
			<td colspan="8" style="height:32px" align="center">
				<a href="?page=1"><img src="/user/skin/images/page_first.gif" border="0" /></a>
				<a href="?page=<%=page_prev%>"><img src="/user/skin/images/page_prev.gif" border="0" /></a>
				<%for(int i=1+(page_range_start*5);i<5+(page_range_start*5);i++){
					if(i>total_page) break;
				%>
				<a href="?page=<%=i%>"><%=i%></a>
				<%
				}
				%>
				<a href="?page=<%=page_next%>"><img src="/user/skin/images/page_next.gif" border="0" /></a>
				<a href="?page=<%=total_page%>"><img src="/user/skin/images/page_last.gif" border="0" /></a>
			</td>
		</tr>
	</table>
</body>
</html>