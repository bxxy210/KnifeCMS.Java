<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%@include file="/include/manage/checkUser.jsp" %>
<%
int page_now=1;
int page_prev=1;
int page_next=1;
int total_page=1;
int page_range_start=0;
if(request.getParameter("page")!=null){
	page_now=Integer.parseInt(request.getParameter("page"));
	page_range_start=(int)(page_now/5);
	page_next=page_now;
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>资料类别</title>
<style type="text/css">
body{
	margin:0px;
	padding:0px;
}
.data_list{
	border:1px solid gray;
	width:100%;
	font-size:12px;
}
.data_list th{
	height:32px;
	border-bottom:1px solid gray;
}
.data_list td{
	height:18px;
	text-align:center;
	border-bottom:1px solid gray;
}
</style>
</head>
<body>
	<table class="data_list" cellspacing="0" cellpadding="0">
		<tr>
			<th>编号</th>
			<th>资料类别</th>
			<th>操作</th>
		</tr>
		<%
		DocumenttypeDAO doctDAO=new DocumenttypeDAO();
		List docts = doctDAO.findPagedAll(page_now,20);
		total_page = doctDAO.getTotalPage(20);
		if(page_now>1)page_prev=page_now-1;
		if(page_now<total_page)page_next=page_now+1;
		String applyDate="";
		for(Object aobj:docts){
		Documenttype adoct=(Documenttype)aobj;
		%>
		<tr>
			<td><%=adoct.getId()%></td>
			<td><%=adoct.getType()%></td>
			<td>
				<a href="doctedit.jsp?id=<%=adoct.getId()%>">编辑</a> | 
				<a href="/user/opt/deldoct.jsp?id=<%=adoct.getId()%>">删除</a>
			</td>
		</tr>
		<%}%>
		<tr>
			<td colspan="2" style="height:32px" align="center">
				<a href="?page=1"><img src="/user/skin/images/page_first.gif" border="0" /></a>
				<a href="?page=<%=page_prev%>"><img src="/user/skin/images/page_prev.gif" border="0" /></a>
				<%for(int i=1+(page_range_start*5);i<5+(page_range_start*5);i++){
					if(i>total_page) break;
				%>
				<a href="?page=<%=i%>"><%=i%></a>
				<%
				}
				%>
				<a href="?page=<%=page_next%>"><img src="/user/skin/images/page_next.gif" border="0" /></a>
				<a href="?page=<%=total_page%>"><img src="/user/skin/images/page_last.gif" border="0" /></a>
			</td>
			<td><a href="doctadd.jsp">添加资料类别</a></td>
		</tr>
	</table>
</body>
</html>