<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
int	uid = 0;
if(request.getParameter("uid")!=null){
	uid				= Integer.parseInt(request.getParameter("uid"));
}
if(uid>0){
	UserinfoDAO userDAO = new UserinfoDAO();
	Userinfo newUser = userDAO.findById((long)uid);	
	try{
		//如果是机构会员,则让出机构名额
		if(newUser.getDepartment()!=null){
			Userinfo adminUser=userDAO.getDepartmentAdmin(newUser.getDepartment());
			if(adminUser!=null){
			adminUser.setUsednum(adminUser.getUsednum()-1);
			if(adminUser.getUsednum()<0){
				adminUser.setUsednum(0);
			}
			userDAO.update(adminUser);
			}
		}
		userDAO.delete(newUser);
		msg="删除成功！";
	}catch(Exception e){
		msg="删除失败！";
	}
}else{
	msg="参数无效！";
}
%>
<script language="javascript">
alert("<%=msg%>");
history.back(-1);
</script>