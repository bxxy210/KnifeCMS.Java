<%@ page language="java" import="java.util.*,com.knife.member.*,java.text.SimpleDateFormat,java.sql.Timestamp" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
int id=0;
if(request.getParameter("id")!=null){
	id				= Integer.parseInt(request.getParameter("id"));
}
String title 		= request.getParameter("title");
String host			= request.getParameter("host");
String meetingid	= request.getParameter("meetingid");
String starttime	= request.getParameter("starttime");
String endtime		= request.getParameter("endtime");

Meeting ameeting = new Meeting();
MeetingDAO meetingDAO = new MeetingDAO();
if(id>0){
	ameeting=meetingDAO.findById(id);
}
ameeting.setTitle(title);
ameeting.setHost(host);
ameeting.setMeetingid(meetingid);
SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
try{ 
	Date applyDate = dateFormat.parse(starttime); 
	Timestamp applyTime = new Timestamp(applyDate.getTime());
	ameeting.setStarttime(applyTime);
	applyDate = dateFormat.parse(endtime); 
	applyTime = new Timestamp(applyDate.getTime());
	ameeting.setEndtime(applyTime);
}catch(Exception e){ 
}
ameeting.setIsclosed(false);
try{
	meetingDAO.save(ameeting);
	msg="编辑成功！";
}catch(Exception e){
	msg="编辑失败！";
}
%>
<script language="javascript">
alert("<%=msg%>");
history.back(-1);
</script>