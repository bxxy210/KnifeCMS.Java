<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
String msg="";
Boolean isLogin=false;
Userinfo auser=null;

String email 		= request.getParameter("email");
String password 	= request.getParameter("password");
UserinfoDAO userDAO = new UserinfoDAO();

List<Userinfo> users = userDAO.findByEmail(email);
if(users.size()>0){
	auser=users.get(0);
}
if(auser!=null){
	if(auser.getPassword().equals(password)){
		if(!"admin".equals(auser.getAcount())){
			session.removeAttribute("adminname");
			msg="登陆失败！";
			isLogin=false;
		}else{
			session.setAttribute("adminname",email);
			//msg="登陆成功！";
			isLogin=true;
		}
	}else{
		session.removeAttribute("adminname");
		msg="登陆失败！";
		isLogin=false;
	}
}else{
	session.removeAttribute("adminname");
	msg="登陆失败！";
	isLogin=false;
}
%>
<script language="javascript">
<%
if(isLogin){
	out.println("location.href='/user/admin/index.jsp'");
}else{
	out.println("alert('"+msg+"');");
	out.println("history.back(-1);");
}
%>
</script>