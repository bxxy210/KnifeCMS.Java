<%@ page language="java" import="java.util.*,com.knife.member.*" pageEncoding="UTF-8"%>
<%
request.setCharacterEncoding("UTF-8");
String msg="";
int	id = 0;
if(request.getParameter("id")!=null){
	id				= Integer.parseInt(request.getParameter("id"));
}
if(id>0){
	RisktypeDAO riskDAO = new RisktypeDAO();
	Risktype risk = riskDAO.findById(id);
	try{
		riskDAO.delete(risk);
		msg="删除成功！";
	}catch(Exception e){
		msg="删除失败！";
	}
}else{
	msg="参数无效！";
}
%>
<script language="javascript">
alert("<%=msg%>");
location.href="/user/admin/inc/risktype.jsp";
</script>