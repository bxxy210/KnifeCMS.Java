<%
String id=request.getParameter("id")==null?"":request.getParameter("id");
%><jsp:useBean id="task" scope="session" class="com.knife.service.GenerateNewsTask"/>
    <script language="javascript">
        var isload=true;
		if(timer!=null){
        	clearInterval(timer);
        }
        
        function reloadThis(){
        	isload=false;
	        jQuery('#taskbar').load('/include/system/NewsTask_status.jsp?id=<%=id%>&v=' + (new Date()).getTime(),function(data){
	        	isload=true;
	        });
        }
        
        function startLoad(){
        	if(isload){
        		reloadThis();
        	}
        }
    jQuery.ajaxSetup ({
		cache: false
	});
    function startTask(){
    	jQuery.get('/include/system/NewsTask_start.jsp?id=<%=id%>',function(data){
    		jQuery('#taskbar').load('/include/system/NewsTask_status.jsp?id=<%=id%>&v=' + (new Date()).getTime());
    	});
    }
    function stopTask(){
    	jQuery.get('/include/system/NewsTask_stop.jsp',function(data){
    		jQuery('#taskbar').load('/include/system/NewsTask_status.jsp?id=<%=id%>&v=' + (new Date()).getTime());
    	});
    }
    </script>
    <% if (task.isRunning()) { %>
        <SCRIPT LANGUAGE="javascript">
        jQuery(function(){
			if(timer!=null){
	        	clearInterval(timer);
	        }
        	timer=setInterval("startLoad()",3000);
        });
        </SCRIPT>
    <% } %>
<%
task.setSid(id);
int percent = task.getPercent();
%>
<table width="100%" BORDER="0" CELLPADDING=0 CELLSPACING=0>
<tr>
	<td width="60">生成文章</td>
	<td width="240" valign="top">
    <TABLE WIDTH="100%" height="15" style="border-top:1px solid black;border-left:1px solid black;border-right:1px solid #F1F1F1;border-bottom:1px solid #F1F1F1" BORDER=0 CELLPADDING=0 CELLSPACING=0>
        <TR>
    	<% if (task.isRunning()) { %>
        <%if(percent>0){%>
            <TD WIDTH="<%=percent%>%" BGCOLOR="#000080"></TD>
        <%} %>
        <%if(percent<100){%>
            <TD WIDTH="<%=(100-percent)%>" BGCOLOR="gray"></TD>
        <%} %>
        <% } else { %>
        	<TD align="center" BGCOLOR="gray"><%=percent%>%</TD>
        <% } %>
        </TR>
    </TABLE>
    <!-- br/>total:<%=task.getTotal()%>,count:<%=task.getCounter()%>,percent:<%=percent%> -->
	</td>
	<TD>
		<% if (task.isRunning()) { %>
        	<img style="cursor:pointer" src="/include/system/img/stop.png" onclick="stopTask()"/>
        <% } else { %>
        	<img style="cursor:pointer" src="/include/system/img/start.png" onclick="startTask()"/>
        <% } %>
    </TD>
    </TR>
</TABLE>