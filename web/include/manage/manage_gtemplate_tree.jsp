<%@ page language="java"  pageEncoding="UTF-8"  import="com.knife.news.model.Constants,com.knife.news.object.User"%>
<%@ include file="checkUser.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>文件列表</title>
		<link rel="stylesheet" href="/include/manage/zTreeStyle/zTreeStyle.css" type="text/css">
<script type="text/javascript" src="/include/manage/js/jquery.js"></script>
<script type="text/javascript" src="/include/manage/js/jquery-ztree.js"></script>
<script type="text/javascript">
<!--
$.ajaxSetup ({  
	cache: false
}); 

function clone(jsonObj, newName) {
    var buf;
    if (jsonObj instanceof Array) {
        buf = [];
        var i = jsonObj.length;
        while (i--) {
            buf[i] = clone(jsonObj[i], newName);
        }
        return buf;
    }else if (typeof jsonObj == "function"){
        return jsonObj;
    }else if (jsonObj instanceof Object){
        buf = {};
        for (var k in jsonObj) {
	        if (k!="parentNode") {
	            buf[k] = clone(jsonObj[k], newName);
	            if (newName && k=="name") buf[k] += newName;
	        }
        }
        return buf;
    }else{
        return jsonObj;
    }
}
  
	var zTree1;
	var setting;

	setting = {
		async: true,
		asyncUrl: "getGTemplate.jsp",
		asyncParam: ["name", "id", "siteId"],
		callback: {
			click: zTreeOnClick,
			rightClick: zTreeOnRightClick
		}
	};

	var zNodes =[{siteId:""}];

	$(document).ready(function(){
		refreshTree();
		$("body").bind("mousedown", 
			function(event){
				if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
					$("#rMenu").hide();
				}
			});
	});

	function showRMenu(type, x, y) {
		$("#rMenu ul").show();
		if (type=="root") {
			$("#m_del").hide();
		}
		$("#rMenu").css({"top":y+"px", "left":x+"px", "display":"block"});
	}
	function hideRMenu() {
		$("#rMenu").hide();
	}

	function zTreeOnRightClick(event, treeId, treeNode) {
		if (!treeNode) {
			zTree1.cancelSelectedNode();
			showRMenu("root", event.clientX, event.clientY);
		} else if (treeNode && !treeNode.noR) {
			if (treeNode.newrole && event.target.tagName != "a" && $(event.target).parents("a").length == 0) {
				zTree1.cancelSelectedNode();
				showRMenu("root", event.clientX, event.clientY);
			} else {
				zTree1.selectNode(treeNode);
				showRMenu("node", event.clientX, event.clientY);
			}
		}
	}
	
	function expandAll(expandSign) {
		zTree1.expandAll(expandSign);
	}

	function refreshTree() {
		hideRMenu();
		zTree1 = $("#treeList").zTree(setting);
	}
	
	function zTreeOnClick(event, treeId, treeNode){
		if(treeNode.isParent){
			window.parent.frames["config"].location.href="/manage_gtemplate.ad?parameter=list&sid="+ treeNode.siteId + "&id=" + treeNode.id;
		}else{
			var tid="";
			if(treeNode.parentNode){
				tid = treeNode.parentNode.id;
			}
			window.parent.frames["config"].location.href="/manage_gtemplate.ad?parameter=prepare&id="+ treeNode.siteId + "&name=" + treeNode.id;
		}
	}
  //-->
</script>
<style type="text/css">
body {
 padding: 0px;
 margin: 0px;
 font-size:12px;
}
#treeTitle{
	font-weight:bold;
	background:url('/include/manage/images/main/tab_05.gif');
	/*width:165px;*/
	height:28px;
	margin:3px;
	margin-bottom:0;
	border-left:1px solid #b5d6e6;
	border-right:1px solid #b5d6e6;
	text-indent:15px;
}
#treeTitle a{line-height:28px;color:black;text-decoration:none}
#treeList {
	margin:3px;
	margin-top:0;
	/*width:165px;*/
	height:482px;
	border:1px solid #b5d6e6;
	border-top:0;
	overflow:hidden;
	overflow-y:auto;
}
.tree{padding:0;}
/* ------------- 右键菜单 -----------------  */

div#rMenu {
	background-color:#F9F9F9;
	text-align: left;
	width:72px;
	border-top:1px solid #F1F1F1;
	border-left:1px solid #F1F1F1;
	border-right:1px solid gray;
	border-bottom:1px solid gray;
}

div#rMenu ul {
	list-style: none outside none;
	margin:0px 1px;
	padding:0;
}
div#rMenu ul li {
	cursor: pointer;
	width:70px;
	height:20px;
	line-height:20px;
	text-indent:8px;
	margin:1px 0px;
	font-size:12px;
	border-bottom:1px solid gray;
}
div#rMenu ul li a{display:block;width:100%;text-decoration:none;color:black}
div#rMenu ul li a:hover{
	color:white;
	background-color:darkblue;
}
div#rMenu ul li a:visited{color:black}
</style>
	</head>
<body>
<div id="treeTitle"><img src="/include/manage/zTreeStyle/img/sim/tree.png" align="absmiddle"/>&nbsp;<a href="/manage_template.ad?parameter=list" target="config">所有文件</a></div>
<div id="treeList" class="tree"></div>
<div id="rMenu" style="position:absolute; display:none;">
	<ul id="m_add"><li><a href="javascript:void(0)">增加文件</a></li></ul>
</div>
	</body>
</html>