<%@ page language="java" pageEncoding="UTF-8"
	import="java.util.*,java.io.*,com.knife.service.HTMLGenerater,com.knife.util.CommUtil"%>
<%@ page
	import="com.knife.news.logic.*,com.knife.news.logic.impl.*,com.knife.web.Globals"%>
<%@ page import="com.knife.news.object.*,com.knife.tools.CopyDirectory"%>
<%@ include file="checkUser.jsp"%><html>
	<head>
		<title>KnifeCMS更新皮肤</title>
		<style>
		body {
			font-size: 12px;
			background: black;
			color: white
		}
		</style>
	</head>
	<body>
		<%
			String sid = "";
			if (request.getParameter("sid") != null) {
				sid = request.getParameter("sid");
			}
			if (sid.length() > 0) {
				SiteService siteDAO = SiteServiceImpl.getInstance();
				TypeService typeDAO = TypeServiceImpl.getInstance();
				Site gsite = siteDAO.getSiteById(sid);

				List<Type> gtypes = typeDAO.getAllTypes(sid);
				out.println("[即将更新]" + gsite.getName() + "下的皮肤<br/>");
				out.flush();

				String url1 = Globals.APP_BASE_DIR + Globals.DEFAULT_TEMPLATE_PATH + File.separator
						+ "web" + File.separator + gsite.getTemplate()
						+ File.separator + "skin" + File.separator;
				String url2 = Globals.APP_BASE_DIR + File.separator + "html"
						+ File.separator + gsite.getPub_dir() + File.separator+ "skin" +File.separator;
				out.println(url1);
				out.println(url2);
				// 获取源文件夹当前下的文件或目录
				File oldFile = new File(url1);
				if (oldFile.exists()) {
					File[] file = oldFile.listFiles();
					if(gsite.getHtml()==1){
						// 创建目标文件夹
						File newFile = new File(url2);
						if (!newFile.exists()) {
							newFile.mkdirs();
						}
						for (int i = 0; i < file.length; i++) {
							if (file[i].isFile()) {
								CopyDirectory.copyFile(file[i], new File(url2 + file[i].getName()));
								if(gsite.getBig5()==1){
									String big5url=url2.replace(gsite.getPub_dir() + File.separator + "skin", gsite.getPub_dir() + File.separator + "big5" + File.separator + "skin");
									CopyDirectory.copyFile(file[i], new File(big5url + file[i].getName()));
								}
								out.println("[拷贝文件]" + file[i].getName() + "<br/>");
								out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
								out.flush();
							}
							if (file[i].isDirectory()) {
								// 复制目录
								String sourceDir = url1 + File.separator
										+ file[i].getName();
								String targetDir = url2 + File.separator
										+ file[i].getName();
								CopyDirectory.copyDirectiory(sourceDir, targetDir);
								if(gsite.getBig5()==1){
									String big5dir=targetDir.replace(gsite.getPub_dir() + File.separator + "skin", gsite.getPub_dir() + File.separator + "big5" + File.separator + "skin");
									CopyDirectory.copyDirectiory(sourceDir, big5dir);
								}
								out.println("[拷贝目录]" + file[i].getName() + "<br/>");
								out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
								out.flush();
							}
						}
					
						//拷贝到发布目录
						url2 = Globals.APP_BASE_DIR + File.separator + "wwwroot"
						+ File.separator + gsite.getPub_dir() + File.separator+ "skin" +File.separator;
						for (int i = 0; i < file.length; i++) {
							if (file[i].isFile()) {
								// 复制文件
								CopyDirectory.copyFile(file[i], new File(url2
										+ file[i].getName()));
								if(gsite.getBig5()==1){
									String big5url=url2.replace(gsite.getPub_dir() + File.separator + "skin", gsite.getPub_dir() + File.separator + "big5" + File.separator + "skin");
									CopyDirectory.copyFile(file[i], new File(big5url + file[i].getName()));
								}
								out.println("[拷贝文件]" + file[i].getName() + "<br/>");
								out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
								out.flush();
							}
							if (file[i].isDirectory()) {
								// 复制目录
								String sourceDir = url1 + File.separator
										+ file[i].getName();
								String targetDir = url2 + File.separator
										+ file[i].getName();
								CopyDirectory.copyDirectiory(sourceDir, targetDir);
								if(gsite.getBig5()==1){
									String big5dir=targetDir.replace(gsite.getPub_dir() + File.separator + "skin", gsite.getPub_dir() + File.separator + "big5" + File.separator + "skin");
									CopyDirectory.copyDirectiory(sourceDir, big5dir);
								}
								out.println("[拷贝目录]" + file[i].getName() + "<br/>");
								out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
								out.flush();
							}
						}
					}else{
						url2 = Globals.APP_BASE_DIR + File.separator+ "skin" +File.separator;
						File newFile = new File(url2);
						if (!newFile.exists()) {
							newFile.mkdirs();
						}
						for (int i = 0; i < file.length; i++) {
							if (file[i].isFile()) {
								// 复制文件
								CopyDirectory.copyFile(file[i], new File(url2
										+ file[i].getName()));
								out.println("[拷贝文件]" + file[i].getName() + "<br/>");
								out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
								out.flush();
							}
							if (file[i].isDirectory()) {
								// 复制目录
								String sourceDir = url1 + File.separator
										+ file[i].getName();
								String targetDir = url2 + File.separator
										+ file[i].getName();
								CopyDirectory.copyDirectiory(sourceDir, targetDir);
								out.println("[拷贝目录]" + file[i].getName() + "<br/>");
								out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
								out.flush();
							}
						}
					}
					out.println("更新完毕！");
					out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				} else {
					out.println("皮肤不存在！");
					out.println("<script>document.body.scrollTop=document.body.scrollHeight;</script>");
				}
			}
		%>
	</body>
</html>