<%@ page language="java" pageEncoding="UTF-8" import="java.util.*,java.text.SimpleDateFormat"%>
<%
Date nowDate=new Date();
SimpleDateFormat sf=new SimpleDateFormat("yyyy年M月d日H:mm:ss");
String nowStr=sf.format(nowDate);
Calendar cal = Calendar.getInstance();
int nowYear		= cal.get(Calendar.YEAR);
int nowMonth	= cal.get(Calendar.MONTH);
int nowDay		= cal.get(Calendar.DAY_OF_MONTH);
int nowHour		= cal.get(Calendar.HOUR_OF_DAY);
int nowMinute	= cal.get(Calendar.MINUTE);
int nowSecond	= cal.get(Calendar.SECOND);
//System.out.println(nowYear + " " + nowMonth +" "+nowDay+" "+nowHour+" "+nowMinute+" "+nowSecond);
%><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>后台管理首页</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
body{ margin:0px; FONT-SIZE: 12px;text-decoration: none; overflow:hidden}
table{ border:0px; }
td{ font:normal 12px 宋体; }
img{ vertical-align:bottom; border:0px; }
a{ font:normal 12px 宋体; color:#000000; text-decoration:none; }
a:hover{ color:#cc0000;text-decoration:underline; }
input,textarea{border:1px solid gray}
.sec_menu{ border-left:1px solid white; border-right:1px solid white; border-bottom:1px solid white; overflow:hidden; background:#FFFFCC; }
.menu_title{ }
.menu_title span  { position:relative; top:0px; left:20px; color:#39867B; font-weight:bold; }
.menu_title2  { }
.menu_title2 span  { position:relative; top:2px; left:8px; color:#cc0000; font-weight:bold; }

.infoform{margin:8px;}
.infoform ul li{margin:8px;}
#bugresult{margin:8px;}
#new_info{width:100%;height:20px;overflow:hidden;color:gray}
#new_info li{list-style:none;margin:0;padding:0;display:block;height:20px;line-height:20px;}
#new_info li a{color:green;}
#alert{color:red;}
</style>
<script src="/include/manage/js/jquery.js" type="text/javascript"></script>
<script language="javascript">
jQuery.ajaxSetup ({
	cache: false
});

$(function(){
	$.getJSON("getInfo.jsp", {TopicCount:10,TitleLen:28,Showtime:1,icon:'&middot;'}, function(data) {
		if(data!=null){
			$('#new_info')[0].innerHTML="";
		}
		$.each(data, function(i,item){
			//$("<img/>").attr("src", item.media.m).appendTo("#images");
			//if ( i == 3 ) return false;
			if(item!=null){
				$('#new_info')[0].innerHTML+=item;
			}
		});
		linkMarquee(20,100,5000);
	});
});

function linkMarquee(lh,speed,delay){
    var t;
    var p = false;
    o=$('#new_info');
    o.html(o.html()+o.html());
    o.hover(function(){p=true;},function(){p=false;});
    o.scrollTop(0);
    function start(){
        t=setInterval(scrolling,speed);
        if(!p){ o.scrollTop(o.scrollTop()+1);} 
    } 
    function scrolling(){ 
        if(o.scrollTop()%lh!=0){
            o.scrollTop(o.scrollTop()+1); 
            if(o.scrollTop()>=o[0].scrollHeight/2) o.scrollTop(0);
        }else{ 
            clearInterval(t);
            setTimeout(start,delay); 
        }
    }
    setTimeout(start,delay);
}

function BugSubmit(){
	/*try{
		$("#bugresult").load("http://knifecms.kmdns.net:8080/bug_report.php");
	}catch(e){
		$("#bugresult")[0].innerHTML="无法提交到BUG反馈系统，可能是网络出了点问题，请<a href=\"mailto:zergskj@163.com\">点击这里</a>邮件给我们";
	}*/
	if($('#bug_title').val().length==0){
			alert("问题标题不能为空！");
			$('#bug_title')[0].focus();
			return;
	}
	if($('#bug_content').val().length==0){
			alert("问题内容不能为空！");
			$('#bug_content')[0].focus();
			return;
	}
	//window.location.href="mailto:zergskj@163.com?subject="+encodeURIComponent($('#bug_title').val())+"&body="+encodeURIComponent($('#bug_content').val());
}
</script>
<script language="javascript">
$(function(){
	var sDate=new Date(<%=nowYear%>,<%=nowMonth%>,<%=nowDay%>,<%=nowHour%>,<%=nowMinute%>,<%=nowSecond%>);
	var stime=sDate.getTime();
	var clientTime=new Date();
	var ctime=clientTime.getTime();
	$('#clienttime')[0].innerHTML=clientTime.toLocaleDateString()+clientTime.toLocaleTimeString();
	if(stime>=(ctime+5000) || stime<=(ctime-5000)){
		$('#alert')[0].innerHTML="警告:您的系统时间与服务器时间不同，可能会导致文章发布问题:"+sDate.toLocaleDateString()+sDate.toLocaleTimeString();
	}else{
		$('#info')[0].innerHTML="系统正常运行";
	}
});
</script>
</head>
<body>
<div style="margin:12px;border:1px solid gray">
<table cellpadding="2" cellspacing="1" border="0" width="98%" class="border" align=center>
	<tr>
		<td width="80"><h4>最新动态</h4></td>
    <td>
			<div id="new_info"><li>数据载入中……</li></div>
		</td>
	</tr>
</table>
<table cellpadding="2" cellspacing="1" border="0" width="98%" class="border" align=center>
	<tr>
		<td><h4>基本信息</h4></td>
	</tr>
	<tr>
		<td>
			<div class="infoform">
				<ul>
					<li>服务器当前时间：<span id="servertime"><%=nowStr%></span></li>
					<li>本　地当前时间：<span id="clienttime"></span></li>
					<li>友情提示：<span id="alert"></span><span id="info"></span></li>
				</ul>
			</div>
		</td>
	</tr>
	<tr>
		<td><h4>问题反馈</h4></td>
	</tr>
  <tr> 
    <td>
				<div class="infoform">
					<ul>
						<li>为了系统的改进与完善，请不要在公共场合讨论任何与KnifeCMS相关的bug，谢谢！</li>
						<!--
						<li>问题标题:<input id="bug_title" type="text" name="bug_title" /></li>
						<li>问题内容:<textarea id="bug_content" name="bug_content"></textarea></li>
						<li>
							<input type="button" name="button" value="提交" onClick="BugSubmit()" />
							<input type="button" name="reset" value="重置" />
						</li>
						-->
						<li>您在使用中遇到任何疑问和想法，均可提交到<a href="http://www.knifecms.com/bbs" target="_blank">KnifeCMS论坛</a></li>
						<li>您也可将问题提交到邮箱<a href="mailto:zergskj@163.com">zergskj@163.com</a></li>
					</ul>
				</div>
		</td>
		</tr>
	<tr>
		<td><h4>联系我们</h4></td>
	</tr>
	<tr>
		<td>
			<div class="infoform">
				<ul>
				<li>在线QQ：<a target=blank href="tencent://message/?uin=710633139&Site=http://www.knifecms.com&Menu=yes"><img border="0" SRC="http://wpa.qq.com/pa?p=1:710633139:1" title="点击用QQ与我联系"></a> <!--a target=blank href="tencent://message/?uin=184263203&Site=http://www.knifecms.com&Menu=yes"><img border="0" SRC="http://wpa.qq.com/pa?p=1:184263203:1" title="点击用QQ与我联系"></a--></li>
				<li>咨询电话：010-81512711 (周一至周五 9:00-18:00)</li>
				<li>紧急服务：13911415506</li>
				</ul>
			</div>
      </td>
</tr>
</table>
</div>
<div align="center">
<br>
  Copyright &copy; 2012 <a href='http://www.knifecms.com'><font Arial, Helvetica, sans-serif>KnifeCMS.COM</font></a> 
  All Rights Reserved.<BR>
  <BR>
</div>
</body>
</html>