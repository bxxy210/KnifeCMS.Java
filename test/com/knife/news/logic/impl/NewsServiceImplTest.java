package com.knife.news.logic.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.knife.news.logic.NewsService;
import com.knife.news.object.News;

public class NewsServiceImplTest {
	NewsService newsDAO=new NewsServiceImpl();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testGetAllTopNewsStringInt() {
		List<News> a=newsDAO.getAllTopNews("1668386-21457058", 3);
		for(int i=0;i<a.size();i++){
			News b=(News)a.get(i);
			System.out.println(b.getTitle());
		}
		assertNotNull(a);
		assertEquals(a.size(), 3);
		//fail("Not yet implemented");
	}

}
